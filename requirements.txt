Django==1.8.5
django-admin-bootstrapped==2.5.6
django-braces==1.8.1
django-extensions==1.5.7
django-filter==0.11.0
django-model-utils==2.3.1
# django-mongoengine==0.2.1 # pip install git+https://github.com/MongoEngine/django-mongoengine.git
django-mptt==0.8.0
# django-rest-framework-mongoengine==3.3.0 # pip install git+https://github.com/umutbozkurt/django-rest-framework-mongoengine.git
django-rest-swagger==0.3.4
djangorestframework==3.3.2
django-reversion==1.9.3
futures==3.0.3
mongoengine==0.10.6  # 0.8.7, 0.10.5
psycopg2==2.6.1
pymongo==2.8.1  # 2.7.2, 2.8.1, 3.0.3 has problem, 3.2.1 has problem
pyparsing==2.0.3
python-dateutil==2.4.2
pytz==2015.4
# pywin32==219
PyYAML==3.11
reportlab==3.2.0
requests==2.9.1
# scikit-image==0.11.3
singledispatch==3.4.0.3
six==1.9.0
slugify==0.0.1
