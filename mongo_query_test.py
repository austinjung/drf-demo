import sys
import os

sys.path.extend([os.getcwd()])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_demo.settings")

import django

django.setup()

from kaizen_api.models import *

import timeit


def get_email_sent_log():
    return EmailSentLog.objects.filter(subject__icontains='golden standard', email__icontains='ibcontrols')


def get_email_sent_log_except_email():
    return EmailSentLog.objects.filter(subject__icontains='golden standard', email__icontains='ibcontrols').exclude(
        'email')


def get_email_failed_delivery():
    return EmailFailedDelivery.objects.filter(message__to__icontains='gschnider')


def get_email_failed_delivery_with_selected_fields():
    return EmailFailedDelivery.objects.filter(message__to__icontains='gschnider').fields(['message__to, message__email_from'])


def get_loop_through_all_email_sent_log():
    for log in get_email_sent_log():
        pass


def get_loop_through_all_email_sent_log_except_email():
    for log in get_email_sent_log_except_email():
        pass


def get_loop_through_all_email_failed_delivery():
    for log in get_email_failed_delivery():
        pass


def get_loop_through_all_email_failed_delivery_with_selected_fields():
    for log in get_email_failed_delivery_with_selected_fields():
        pass


def get_loop_through_all_inventories():
    for inventory in Inventory.objects.all():
        pass


logs = EmailSentLog.objects.filter(subject__icontains='golden standard', email__icontains='ibcontrols').exclude('email')
for log in logs:
    if log.email:
        print log.email + ' : ' + log.subject
    else:
        print log.subject
print len(logs)

logs = EmailSentLog.objects.filter(subject__icontains='golden standard', email__icontains='ibcontrols').only('email')
for log in logs:
    print log.email
print len(logs)

logs = EmailFailedDelivery.objects.filter(message__to__icontains='gschnider').fields(['message__to, message__email_from'])
for log in logs:
    print log.message.to + '<--' + log.message.email_from
print len(logs)

if __name__ == '__main__':
    # print 'get_email_sent_log'
    # print([timeit.timeit('get_email_sent_log()', number=1, setup='from __main__ import get_email_sent_log') for x in range(0, 5)])
    #
    # print 'get_email_sent_log_except_email'
    # print([timeit.timeit('get_email_sent_log_except_email()', number=1, setup='from __main__ import get_email_sent_log_except_email') for x in range(0, 5)])
    #
    # print 'get_email_failed_delivery'
    # print([timeit.timeit('get_email_failed_delivery()', number=1, setup='from __main__ import get_email_failed_delivery') for x in range(0, 5)])
    #
    # print 'get_email_failed_delivery_with_selected_fields'
    # print([timeit.timeit('get_email_failed_delivery_with_selected_fields()', number=1, setup='from __main__ import get_email_failed_delivery_with_selected_fields') for x in range(0, 5)])
    #
    # print 'get_loop_through_all_email_sent_log'
    # print([timeit.timeit('get_loop_through_all_email_sent_log()', number=1, setup='from __main__ import get_loop_through_all_email_sent_log') for x in range(0, 5)])
    #
    # print 'get_loop_through_all_email_sent_log_except_email'
    # print([timeit.timeit('get_loop_through_all_email_sent_log_except_email()', number=1, setup='from __main__ import get_loop_through_all_email_sent_log_except_email') for x in range(0, 5)])
    #
    # print 'get_loop_through_all_email_failed_delivery'
    # print([timeit.timeit('get_loop_through_all_email_failed_delivery()', number=1, setup='from __main__ import get_loop_through_all_email_failed_delivery') for x in range(0, 5)])
    #
    # print 'get_loop_through_all_email_failed_delivery_with_selected_fields'
    # print([timeit.timeit('get_loop_through_all_email_failed_delivery_with_selected_fields()', number=1, setup='from __main__ import get_loop_through_all_email_failed_delivery_with_selected_fields') for x in range(0, 5)])

    print 'get_loop_through_all_inventories'
    print([timeit.timeit('get_loop_through_all_inventories()', number=1, setup='from __main__ import get_loop_through_all_inventories') for x in range(0, 5)])
