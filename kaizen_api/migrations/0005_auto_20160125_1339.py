# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0004_build_mpttobjects_tree'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reports',
            name='building',
            field=models.ForeignKey(related_name='building_reports', blank=True, to='kaizen_api.Buildings', null=True),
        ),
    ]
