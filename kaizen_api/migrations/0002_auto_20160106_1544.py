# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='authcas',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='authevent',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='authgroup',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='authmembership',
            name='group',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthGroup', null=True),
        ),
        migrations.AddField(
            model_name='authmembership',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='authpermission',
            name='group',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthGroup', null=True),
        ),
        migrations.AddField(
            model_name='authuser',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='buildings',
            name='building_type_0',
            field=models.ForeignKey(db_column='building_type_id', blank=True, to='kaizen_api.BuildingTypes', null=True),
        ),
        migrations.AddField(
            model_name='buildings',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='buildings',
            name='timezone',
            field=models.ForeignKey(blank=True, to='kaizen_api.Timezones', null=True),
        ),
        migrations.AddField(
            model_name='buildingtypes',
            name='price_factor',
            field=models.ForeignKey(blank=True, to='kaizen_api.BuildingTypePriceFactors', null=True),
        ),
        migrations.AddField(
            model_name='chartenergymeters',
            name='chart_instance',
            field=models.ForeignKey(blank=True, to='kaizen_api.ChartInstances', null=True),
        ),
        migrations.AddField(
            model_name='chartenergymeters',
            name='energy_meter',
            field=models.ForeignKey(blank=True, to='kaizen_api.EnergyMeters', null=True),
        ),
        migrations.AddField(
            model_name='chartinstances',
            name='chart',
            field=models.ForeignKey(blank=True, to='kaizen_api.Charts', null=True),
        ),
        migrations.AddField(
            model_name='chartinstances',
            name='data_set',
            field=models.ForeignKey(blank=True, to='kaizen_api.DataSets', null=True),
        ),
        migrations.AddField(
            model_name='charts',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='charts',
            name='dataflow',
            field=models.ForeignKey(db_column='dataflow', blank=True, to='kaizen_api.Flows', null=True),
        ),
        migrations.AddField(
            model_name='charts',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='charts',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='chartslibrary',
            name='dataflow',
            field=models.ForeignKey(db_column='dataflow', blank=True, to='kaizen_api.FlowsLibrary', null=True),
        ),
        migrations.AddField(
            model_name='chartslibrary',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='chartslibrary',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='chartslibraryratings',
            name='chart_library',
            field=models.ForeignKey(blank=True, to='kaizen_api.ChartsLibrary', null=True),
        ),
        migrations.AddField(
            model_name='chartslibraryratings',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='clients',
            name='accounting_contact',
            field=models.ForeignKey(db_column='accounting_contact', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='clients',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='clients',
            name='sales_contact',
            field=models.ForeignKey(related_name='sales_contact_clients', db_column='sales_contact', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='clients',
            name='technical_contact',
            field=models.ForeignKey(related_name='technical_contact_clients', db_column='technical_contact', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='datasets',
            name='flow',
            field=models.ForeignKey(blank=True, to='kaizen_api.Flows', null=True),
        ),
        migrations.AddField(
            model_name='datasetseries',
            name='data_set',
            field=models.ForeignKey(blank=True, to='kaizen_api.DataSets', null=True),
        ),
        migrations.AddField(
            model_name='devicesettings',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='energycustomunits',
            name='client',
            field=models.ForeignKey(blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='energymeterconfig',
            name='meter',
            field=models.OneToOneField(default=None, to='kaizen_api.EnergyMeters'),
        ),
        migrations.AddField(
            model_name='energymeters',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='energyoatref',
            name='meter_ref',
            field=models.ForeignKey(db_column='meter_ref', default=None, to='kaizen_api.EnergyMeters'),
        ),
        migrations.AddField(
            model_name='faults',
            name='insight_rule',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightRules', null=True),
        ),
        migrations.AddField(
            model_name='faultslibrary',
            name='insight_rule_library',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightRulesLibrary', null=True),
        ),
        migrations.AddField(
            model_name='favorites',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='flows',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='flows',
            name='insight_class',
            field=models.ForeignKey(db_column='insight_class', blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='flows',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='flowslibrary',
            name='insight_class',
            field=models.ForeignKey(db_column='insight_class', blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='flowslibrary',
            name='object',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='insightfiltersettings',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='insightfiltersettings',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='insightfiltersettings',
            name='insight_filter',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightFilters', null=True),
        ),
        migrations.AddField(
            model_name='insightfiltersettings',
            name='priority',
            field=models.ForeignKey(blank=True, to='kaizen_api.Priorities', null=True),
        ),
        migrations.AddField(
            model_name='insightfiltersettings',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='insightquickfilters',
            name='user',
            field=models.ForeignKey(default=None, to='kaizen_api.AuthUser'),
        ),
        migrations.AddField(
            model_name='insightruleinstances',
            name='data_set',
            field=models.ForeignKey(blank=True, to='kaizen_api.DataSets', null=True),
        ),
        migrations.AddField(
            model_name='insightruleinstances',
            name='insight_rule',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightRules', null=True),
        ),
        migrations.AddField(
            model_name='insightruleinstances',
            name='priority',
            field=models.ForeignKey(blank=True, to='kaizen_api.Priorities', null=True),
        ),
        migrations.AddField(
            model_name='insightrules',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='insightrules',
            name='dataflow',
            field=models.ForeignKey(db_column='dataflow', blank=True, to='kaizen_api.Flows', null=True),
        ),
        migrations.AddField(
            model_name='insightrules',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='insightrules',
            name='priority',
            field=models.ForeignKey(blank=True, to='kaizen_api.Priorities', null=True),
        ),
        migrations.AddField(
            model_name='insightrules',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibrary',
            name='dataflow',
            field=models.ForeignKey(db_column='dataflow', blank=True, to='kaizen_api.FlowsLibrary', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibrary',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibrary',
            name='priority',
            field=models.ForeignKey(blank=True, to='kaizen_api.Priorities', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibrary',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibraryratings',
            name='insight_library',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightRulesLibrary', null=True),
        ),
        migrations.AddField(
            model_name='insightruleslibraryratings',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='inventory',
            name='client_bought',
            field=models.ForeignKey(related_name='bought_inventories', db_column='client_bought', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='inventory',
            name='client_deployed',
            field=models.ForeignKey(related_name='deployed_inventories', db_column='client_deployed', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='mybuildings',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='mybuildings',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='objects',
            name='object_type',
            field=models.ForeignKey(blank=True, to='kaizen_api.ObjectTypes', null=True),
        ),
        migrations.AddField(
            model_name='objects',
            name='parent',
            field=models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True),
        ),
        migrations.AddField(
            model_name='reportartifacts',
            name='report',
            field=models.ForeignKey(blank=True, to='kaizen_api.Reports', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='chart_instance',
            field=models.ForeignKey(blank=True, to='kaizen_api.ChartInstances', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='client',
            field=models.ForeignKey(related_name='client_reports', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='partner',
            field=models.ForeignKey(related_name='partner_reports', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='reportsubscriptions',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='reportsubscriptions',
            name='group',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthGroup', null=True),
        ),
        migrations.AddField(
            model_name='reportsubscriptions',
            name='report',
            field=models.ForeignKey(blank=True, to='kaizen_api.Reports', null=True),
        ),
        migrations.AddField(
            model_name='reportsubscriptions',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='salesorderlineitems',
            name='order',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesOrders', null=True),
        ),
        migrations.AddField(
            model_name='salesorders',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='salesorders',
            name='quote',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesQuotes', null=True),
        ),
        migrations.AddField(
            model_name='salesorders',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='salesquotelineitems',
            name='quote',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesQuotes', null=True),
        ),
        migrations.AddField(
            model_name='salesquotes',
            name='archive_reason',
            field=models.ForeignKey(db_column='archive_reason', blank=True, to='kaizen_api.SalesQuoteLostReasons', null=True),
        ),
        migrations.AddField(
            model_name='salesquotes',
            name='client',
            field=models.ForeignKey(related_name='client_quotes', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='salesquotes',
            name='partner',
            field=models.ForeignKey(related_name='partner_quotes', blank=True, to='kaizen_api.Clients', null=True),
        ),
        migrations.AddField(
            model_name='salesquotes',
            name='timezone',
            field=models.ForeignKey(blank=True, to='kaizen_api.Timezones', null=True),
        ),
        migrations.AddField(
            model_name='salesquotes',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='salesquotescucubes',
            name='quote',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesQuotes', null=True),
        ),
        migrations.AddField(
            model_name='salesquotescucubes',
            name='tier',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesCucubeTiers', null=True),
        ),
        migrations.AddField(
            model_name='salesquotessystemrows',
            name='quote',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesQuotes', null=True),
        ),
        migrations.AddField(
            model_name='salesquotessystemrows',
            name='system_type',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesSystemTypes', null=True),
        ),
        migrations.AddField(
            model_name='salesspecialpricingrequests',
            name='quote',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesQuotes', null=True),
        ),
        migrations.AddField(
            model_name='salessystemtypes',
            name='category',
            field=models.ForeignKey(blank=True, to='kaizen_api.SalesSystemCategories', null=True),
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='building',
            field=models.ForeignKey(blank=True, to='kaizen_api.Buildings', null=True),
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='group',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthGroup', null=True),
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='insight_class',
            field=models.ForeignKey(blank=True, to='kaizen_api.InsightClasses', null=True),
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='priority',
            field=models.ForeignKey(blank=True, to='kaizen_api.Priorities', null=True),
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='user',
            field=models.ForeignKey(blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='authmembership',
            unique_together=set([('user', 'group')]),
        ),
        migrations.AlterUniqueTogether(
            name='calculatedtrendlogs',
            unique_together=set([('insight_rule_instance_id', 'data_series_id')]),
        ),
    ]
