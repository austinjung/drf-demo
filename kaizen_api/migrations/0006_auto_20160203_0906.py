# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0005_auto_20160125_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='reports',
            name='accessed_by',
            field=models.ForeignKey(related_name='accessed_reports', db_column='accessed_by', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='created_by',
            field=models.ForeignKey(related_name='created_reports', db_column='created_by', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
        migrations.AddField(
            model_name='reports',
            name='modified_by',
            field=models.ForeignKey(related_name='modified_reports', db_column='modified_by', blank=True, to='kaizen_api.AuthUser', null=True),
        ),
    ]
