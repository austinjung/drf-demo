# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0002_auto_20160106_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='MpttObjects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('object', models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='kaizen_api.MpttObjects', null=True)),
            ],
            options={
                'db_table': 'mptt_objects',
                'managed': True,
            },
        ),
    ]
