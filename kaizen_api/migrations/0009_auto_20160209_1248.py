# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0008_auto_20160205_1411'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='buildings',
            options={'managed': True, 'verbose_name_plural': 'Buildings'},
        ),
    ]
