# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AlembicVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version_num', models.CharField(max_length=32)),
            ],
            options={
                'db_table': 'alembic_version',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthCas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(null=True, blank=True)),
                ('service', models.CharField(max_length=512, null=True, blank=True)),
                ('ticket', models.CharField(max_length=512, null=True, blank=True)),
                ('renew', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_cas',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_stamp', models.DateTimeField(null=True, blank=True)),
                ('client_ip', models.CharField(max_length=512, null=True, blank=True)),
                ('origin', models.CharField(max_length=512, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_event',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.CharField(max_length=64, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_group',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'auth_membership',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('table_name', models.CharField(max_length=512, null=True, blank=True)),
                ('record_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_permission',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthToken',
            fields=[
                ('uuid', models.CharField(max_length=512, serialize=False, primary_key=True)),
                ('user_id', models.IntegerField(null=True, blank=True)),
                ('expires', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_token',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=64, null=True, blank=True)),
                ('last_name', models.CharField(max_length=64, null=True, blank=True)),
                ('email', models.CharField(max_length=512, null=True, blank=True)),
                ('password', models.CharField(max_length=512, null=True, blank=True)),
                ('registration_key', models.CharField(max_length=512, null=True, blank=True)),
                ('reset_password_key', models.CharField(max_length=512, null=True, blank=True)),
                ('registration_id', models.CharField(max_length=512, null=True, blank=True)),
                ('work_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('home_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('cell_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('work_position', models.CharField(max_length=32, null=True, blank=True)),
                ('work_title', models.CharField(max_length=32, null=True, blank=True)),
                ('photo', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_photo', models.CharField(max_length=512, null=True, blank=True)),
                ('api_key', models.CharField(max_length=512, null=True, blank=True)),
                ('operations_access', models.CharField(max_length=1)),
                ('sales_access', models.CharField(max_length=1)),
                ('default_application', models.CharField(max_length=512, null=True, blank=True)),
                ('systems_access', models.CharField(max_length=1)),
                ('has_accepted_eula', models.CharField(max_length=1)),
                ('sales_admin', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'auth_user',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Buildings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('address', models.CharField(max_length=512, null=True, blank=True)),
                ('postal_code', models.CharField(max_length=512, null=True, blank=True)),
                ('city', models.CharField(max_length=512, null=True, blank=True)),
                ('prov_state', models.CharField(max_length=512, null=True, blank=True)),
                ('country', models.CharField(max_length=32, null=True, blank=True)),
                ('phone_number', models.CharField(max_length=512, null=True, blank=True)),
                ('fax_number', models.CharField(max_length=512, null=True, blank=True)),
                ('technical_contact', models.CharField(max_length=512, null=True, blank=True)),
                ('technical_contact_phone', models.CharField(max_length=512, null=True, blank=True)),
                ('technical_contact_email', models.CharField(max_length=512, null=True, blank=True)),
                ('logo', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_logo', models.CharField(max_length=512, null=True, blank=True)),
                ('building_type', models.CharField(max_length=512, null=True, blank=True)),
                ('size_sqft', models.IntegerField(null=True, blank=True)),
                ('size_occupancy', models.IntegerField(null=True, blank=True)),
                ('licensed_tls', models.IntegerField(null=True, blank=True)),
                ('weather_station', models.CharField(max_length=512, null=True, blank=True)),
                ('next_trend_instance', models.IntegerField(null=True, blank=True)),
                ('next_structured_view_instance', models.IntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField()),
                ('size_sqmt', models.IntegerField(null=True, blank=True)),
                ('preferred_building_size_unit', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'buildings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='BuildingTypePriceFactors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estimator_building_type', models.CharField(unique=True, max_length=512)),
                ('golden_standard_factor', models.DecimalField(max_digits=12, decimal_places=2)),
                ('trend_log_vault_factor', models.DecimalField(max_digits=12, decimal_places=2)),
                ('device_backup_vault_factor', models.DecimalField(max_digits=12, decimal_places=2)),
                ('energy_factor', models.DecimalField(max_digits=12, decimal_places=2)),
                ('integrity_factor', models.DecimalField(max_digits=12, decimal_places=2)),
            ],
            options={
                'db_table': 'building_type_price_factors',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='BuildingTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(unique=True, max_length=512)),
            ],
            options={
                'db_table': 'building_types',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='CalculatedTrendLogs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('trend_log_instance', models.IntegerField(null=True, blank=True)),
                ('insight_rule_instance_id', models.IntegerField(null=True, blank=True)),
                ('data_series_id', models.IntegerField(null=True, blank=True)),
                ('start_date', models.DateTimeField(null=True, blank=True)),
                ('end_date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'calculated_trend_logs',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ChartEnergyMeters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'chart_energy_meters',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ChartInstances',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('disabled_note', models.TextField(null=True, blank=True)),
                ('date_range', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'chart_instances',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Charts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('disabled_note', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('author_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('sample_image', models.CharField(max_length=512, null=True, blank=True)),
                ('lgx_url', models.CharField(max_length=512, null=True, blank=True)),
                ('data_source', models.IntegerField(null=True, blank=True)),
                ('lgx_file_name', models.CharField(max_length=512, null=True, blank=True)),
                ('major_version', models.SmallIntegerField(null=True, blank=True)),
                ('minor_version', models.SmallIntegerField(null=True, blank=True)),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
            ],
            options={
                'db_table': 'charts',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ChartsLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('author_notes', models.CharField(max_length=512, null=True, blank=True)),
                ('sample_image', models.CharField(max_length=512, null=True, blank=True)),
                ('avg_rating', models.IntegerField(null=True, blank=True)),
                ('name', models.CharField(max_length=128, unique=True, null=True, blank=True)),
                ('data_source', models.IntegerField(null=True, blank=True)),
                ('lgx_url', models.CharField(max_length=512, null=True, blank=True)),
                ('lgx_file_name', models.CharField(max_length=512, null=True, blank=True)),
                ('major_version', models.SmallIntegerField(null=True, blank=True)),
                ('minor_version', models.SmallIntegerField(null=True, blank=True)),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
            ],
            options={
                'db_table': 'charts_library',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ChartsLibraryRatings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'charts_library_ratings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('address', models.CharField(max_length=512, null=True, blank=True)),
                ('postal_code', models.CharField(max_length=512, null=True, blank=True)),
                ('city', models.CharField(max_length=512, null=True, blank=True)),
                ('prov_state', models.CharField(max_length=512, null=True, blank=True)),
                ('country', models.CharField(max_length=32, null=True, blank=True)),
                ('phone_number', models.CharField(max_length=512, null=True, blank=True)),
                ('fax_number', models.CharField(max_length=512, null=True, blank=True)),
                ('logo', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_logo', models.CharField(max_length=512, null=True, blank=True)),
                ('logo_small', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_logo_small', models.CharField(max_length=512, null=True, blank=True)),
                ('logo_footer', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_logo_footer', models.CharField(max_length=512, null=True, blank=True)),
                ('gp_account_id', models.CharField(max_length=16, null=True, blank=True)),
                ('partnership', models.CharField(max_length=16, null=True, blank=True)),
            ],
            options={
                'db_table': 'clients',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DataSets',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'data_sets',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DataSetSeries',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('series_id', models.IntegerField(null=True, blank=True)),
                ('series_override', models.TextField(null=True, blank=True)),
                ('object_name', models.CharField(max_length=512, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_set_series',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DeviceSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cucube_sitename', models.CharField(max_length=32, null=True, blank=True)),
                ('dfirst', models.IntegerField(null=True, blank=True)),
                ('dlast', models.IntegerField(null=True, blank=True)),
                ('cucube_serial', models.CharField(max_length=32768, null=True, blank=True)),
            ],
            options={
                'db_table': 'device_settings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EnergyCustomUnits',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('abbreviation', models.CharField(max_length=6, null=True, blank=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
                ('energy_type', models.IntegerField(null=True, blank=True)),
                ('cascade', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 'energy_custom_units',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EnergyMeterConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('daily_display_interval', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'energy_meter_config',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EnergyMeters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('energy_type', models.IntegerField(null=True, blank=True)),
                ('map_object', models.CharField(max_length=512, null=True, blank=True)),
                ('description', models.CharField(max_length=512, null=True, blank=True)),
                ('suffix', models.CharField(max_length=512, null=True, blank=True)),
                ('parent_id', models.IntegerField(null=True, blank=True)),
                ('meter_type', models.IntegerField(null=True, blank=True)),
                ('is_utility', models.CharField(max_length=1, null=True, blank=True)),
                ('baseline_chart_type', models.CharField(max_length=512, null=True, blank=True)),
                ('target_adjustment', models.SmallIntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'energy_meters',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EnergyOatRef',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('map_object', models.CharField(max_length=512)),
                ('suffix', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
                'db_table': 'energy_oat_ref',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Environments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'environments',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Faults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('possible_fault_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'faults',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='FaultsLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('possible_fault_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'faults_library',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Favorites',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('description', models.CharField(max_length=512, null=True, blank=True)),
                ('url', models.CharField(max_length=512, null=True, blank=True)),
            ],
            options={
                'db_table': 'favorites',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Flows',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('definition', models.TextField(null=True, blank=True)),
                ('designer_info', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('flow_binding', models.IntegerField(null=True, blank=True)),
                ('is_compatible', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'flows',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='FlowsLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('definition', models.TextField(null=True, blank=True)),
                ('designer_info', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('flow_binding', models.IntegerField(null=True, blank=True)),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
            ],
            options={
                'db_table': 'flows_library',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='GoldenStandardConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_type', models.CharField(max_length=128, null=True, blank=True)),
                ('property', models.CharField(max_length=512, null=True, blank=True)),
                ('display_name', models.CharField(max_length=512, null=True, blank=True)),
                ('priority', models.IntegerField(null=True, blank=True)),
                ('property_group', models.IntegerField(null=True, blank=True)),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('alternative_names', models.CharField(max_length=512, null=True, blank=True)),
                ('property_sort', models.IntegerField(null=True, blank=True)),
                ('display_value', models.CharField(max_length=512, null=True, blank=True)),
            ],
            options={
                'db_table': 'golden_standard_config',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightClasses',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('insight_type', models.CharField(max_length=512, null=True, blank=True)),
                ('category', models.CharField(max_length=512, null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_classes',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightFilters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_filters',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightFilterSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'insight_filter_settings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightQuickFilters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('insight_class', models.CharField(max_length=32, null=True, blank=True)),
                ('state_snoozed', models.CharField(max_length=1, null=True, blank=True)),
                ('state_active', models.CharField(max_length=1, null=True, blank=True)),
                ('priority', models.CharField(max_length=16, null=True, blank=True)),
                ('weekday', models.CharField(max_length=32, null=True, blank=True)),
                ('date_range_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_quick_filters',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightRuleInstances',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('insight_message', models.CharField(max_length=512, null=True, blank=True)),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('disabled_note', models.TextField(null=True, blank=True)),
                ('schedule_id', models.IntegerField()),
                ('system_id', models.CharField(max_length=64, null=True, blank=True)),
                ('needs_rerun', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'insight_rule_instances',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightRules',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('disabled_note', models.TextField(null=True, blank=True)),
                ('author_notes', models.CharField(max_length=1028, null=True, blank=True)),
                ('insight_trigger_series', models.IntegerField(null=True, blank=True)),
                ('trigger_type', models.IntegerField(null=True, blank=True)),
                ('date_range', models.IntegerField(null=True, blank=True)),
                ('aggregation_interval', models.IntegerField(null=True, blank=True)),
                ('major_version', models.SmallIntegerField(null=True, blank=True)),
                ('minor_version', models.SmallIntegerField(null=True, blank=True)),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
                ('insight_message', models.CharField(max_length=512, null=True, blank=True)),
                ('description', models.CharField(max_length=1024, null=True, blank=True)),
                ('diagnosis', models.CharField(max_length=1024, null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_rules',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightRulesLibrary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('author_notes', models.CharField(max_length=1028, null=True, blank=True)),
                ('insight_trigger_series', models.IntegerField(null=True, blank=True)),
                ('trigger_type', models.IntegerField(null=True, blank=True)),
                ('date_range', models.IntegerField(null=True, blank=True)),
                ('aggregation_interval', models.IntegerField(null=True, blank=True)),
                ('avg_rating', models.IntegerField(null=True, blank=True)),
                ('name', models.CharField(max_length=128, unique=True, null=True, blank=True)),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
                ('major_version', models.SmallIntegerField(null=True, blank=True)),
                ('minor_version', models.SmallIntegerField(null=True, blank=True)),
                ('insight_message', models.CharField(max_length=512, null=True, blank=True)),
                ('description', models.CharField(max_length=1024, null=True, blank=True)),
                ('diagnosis', models.CharField(max_length=1024, null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_rules_library',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='InsightRulesLibraryRatings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'insight_rules_library_ratings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('serial', models.CharField(max_length=512, null=True, blank=True)),
                ('model', models.CharField(max_length=512, null=True, blank=True)),
                ('mfgserial', models.CharField(max_length=512, null=True, blank=True)),
                ('swversion', models.CharField(max_length=512, null=True, blank=True)),
                ('date_acquired', models.DateField(null=True, blank=True)),
                ('date_sold', models.DateField(null=True, blank=True)),
                ('netcfg', models.TextField(null=True, blank=True)),
                ('oscred', models.TextField(null=True, blank=True)),
                ('vpncred', models.TextField(null=True, blank=True)),
                ('nabto', models.TextField(null=True, blank=True)),
                ('history', models.TextField(null=True, blank=True)),
                ('current_status', models.IntegerField(null=True, blank=True)),
                ('coppercube_name', models.CharField(max_length=64, null=True, blank=True)),
                ('physical_location', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'inventory',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='KaizenConfig',
            fields=[
                ('key', models.CharField(max_length=32, serialize=False, primary_key=True)),
                ('value', models.CharField(max_length=64)),
            ],
            options={
                'db_table': 'kaizen_config',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='MyBuildings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'my_buildings',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Objects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
            ],
            options={
                'db_table': 'objects',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ObjectTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'object_types',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Priorities',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'priorities',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ReportArtifacts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('artifact', models.CharField(max_length=512, null=True, blank=True)),
                ('created_at', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'report_artifacts',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Reports',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, null=True, blank=True)),
                ('schedule_id', models.IntegerField()),
                ('enabled', models.CharField(max_length=1, null=True, blank=True)),
                ('generation_timestamp', models.DateTimeField(null=True, blank=True)),
                ('heading', models.CharField(max_length=512, null=True, blank=True)),
                ('date_range', models.IntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField(null=True, blank=True)),
                ('modified_at', models.DateTimeField(null=True, blank=True)),
                ('accessed_at', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'reports',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ReportSubscriptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'report_subscriptions',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesCucubeTiers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('tl_count', models.IntegerField(null=True, blank=True)),
                ('enterprise', models.CharField(max_length=1)),
                ('cucube_price', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
                ('cucube_lease_price', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
                ('sql_price', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
                ('sql_lease_price', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
            ],
            options={
                'db_table': 'sales_cucube_tiers',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesKaizenPrices',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('price', models.DecimalField(null=True, max_digits=12, decimal_places=3, blank=True)),
            ],
            options={
                'db_table': 'sales_kaizen_prices',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesLineItemsToGpSkus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_name', models.CharField(unique=True, max_length=512)),
                ('official_item_name', models.CharField(max_length=512)),
                ('gp_sku_number', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'sales_line_items_to_gp_skus',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesMeterTiers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meter_count', models.IntegerField()),
                ('meter_price', models.DecimalField(max_digits=12, decimal_places=2)),
            ],
            options={
                'db_table': 'sales_meter_tiers',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesOrderLineItems',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('yearly', models.CharField(max_length=1, null=True, blank=True)),
                ('name', models.CharField(max_length=64)),
                ('quantity', models.IntegerField()),
                ('unit_price', models.DecimalField(max_digits=12, decimal_places=2)),
            ],
            options={
                'db_table': 'sales_order_line_items',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesOrders',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(null=True, blank=True)),
                ('updated_at', models.DateTimeField(null=True, blank=True)),
                ('submitted_at', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_orders',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesQuoteLineItems',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('yearly', models.CharField(max_length=1, null=True, blank=True)),
                ('name', models.CharField(max_length=64)),
                ('quantity', models.IntegerField()),
                ('unit_price', models.DecimalField(max_digits=12, decimal_places=2)),
            ],
            options={
                'db_table': 'sales_quote_line_items',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesQuoteLostReasons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_quote_lost_reasons',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesQuotes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=32, null=True, blank=True)),
                ('created', models.DateTimeField(null=True, blank=True)),
                ('submitted', models.DateTimeField(null=True, blank=True)),
                ('updated', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.DateTimeField(null=True, blank=True)),
                ('expires', models.DateTimeField(null=True, blank=True)),
                ('wizard_finished', models.CharField(max_length=1)),
                ('project_name', models.CharField(max_length=64, null=True, blank=True)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('building_name', models.CharField(max_length=64, null=True, blank=True)),
                ('building_street1', models.CharField(max_length=64, null=True, blank=True)),
                ('building_street2', models.CharField(max_length=64, null=True, blank=True)),
                ('building_city', models.CharField(max_length=64, null=True, blank=True)),
                ('building_state', models.CharField(max_length=64, null=True, blank=True)),
                ('building_country', models.CharField(max_length=64, null=True, blank=True)),
                ('building_zip', models.CharField(max_length=64, null=True, blank=True)),
                ('building_sqft', models.IntegerField(null=True, blank=True)),
                ('building_occupancy', models.IntegerField(null=True, blank=True)),
                ('systems_advanced', models.CharField(max_length=1, null=True, blank=True)),
                ('systems_skipped', models.CharField(max_length=1, null=True, blank=True)),
                ('cucube_lease', models.CharField(max_length=1, null=True, blank=True)),
                ('cucube_sql', models.CharField(max_length=1, null=True, blank=True)),
                ('cucube_sizing', models.CharField(max_length=64, null=True, blank=True)),
                ('service_tl_vault', models.CharField(max_length=1, null=True, blank=True)),
                ('service_obj_vault', models.CharField(max_length=1, null=True, blank=True)),
                ('service_energy', models.CharField(max_length=1, null=True, blank=True)),
                ('service_golden_standard', models.CharField(max_length=1, null=True, blank=True)),
                ('service_fdd', models.CharField(max_length=1, null=True, blank=True)),
                ('partner_multiplier', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
                ('target_margin', models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)),
                ('archive_comments', models.TextField(null=True, blank=True)),
                ('meters_count', models.IntegerField()),
                ('service_kaizen', models.CharField(max_length=1, null=True, blank=True)),
                ('po_number', models.CharField(max_length=32, null=True, blank=True)),
                ('address_code', models.CharField(max_length=15, null=True, blank=True)),
                ('building_sqmt', models.IntegerField(null=True, blank=True)),
                ('preferred_building_size_unit', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_quotes',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesQuotesCucubes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_quotes_cucubes',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesQuotesSystemRows',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=64, null=True, blank=True)),
                ('quantity', models.IntegerField(null=True, blank=True)),
                ('io_count', models.IntegerField(null=True, blank=True)),
                ('tl_count', models.IntegerField(null=True, blank=True)),
                ('obj_count', models.IntegerField(null=True, blank=True)),
                ('fdd_enabled', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'sales_quotes_system_rows',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesSpecialPricingRequests',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(null=True, blank=True)),
                ('updated_at', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=64, null=True, blank=True)),
                ('request_comments', models.TextField(null=True, blank=True)),
                ('response_comments', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_special_pricing_requests',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesSystemCategories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=32, null=True, blank=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
            ],
            options={
                'db_table': 'sales_system_categories',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='SalesSystemTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
                ('io_count', models.IntegerField(null=True, blank=True)),
                ('tl_count', models.IntegerField(null=True, blank=True)),
                ('obj_count', models.IntegerField(null=True, blank=True)),
                ('add_by_default', models.CharField(max_length=1, null=True, blank=True)),
                ('counts_always_editable', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'sales_system_types',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Subscriptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('delivery_method', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'subscriptions',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Timezones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
            ],
            options={
                'db_table': 'timezones',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Web2PySessionShared',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('locked', models.CharField(max_length=1, null=True, blank=True)),
                ('client_ip', models.CharField(max_length=64, null=True, blank=True)),
                ('created_datetime', models.DateTimeField(null=True, blank=True)),
                ('modified_datetime', models.DateTimeField(null=True, blank=True)),
                ('unique_key', models.CharField(max_length=64, null=True, blank=True)),
                ('session_data', models.BinaryField(null=True, blank=True)),
            ],
            options={
                'db_table': 'web2py_session_shared',
                'managed': True,
            },
        ),
    ]
