# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from kaizen_api.models import *


def copy_tables(apps, schema_editor):
    auth_group_local_fields = [f.name for f in KaizenAuthGroup._meta.local_fields]
    for auth_group in AuthGroup.objects.all():
        kaizen_auth_group = KaizenAuthGroup()
        for field_name in auth_group_local_fields:
            setattr(kaizen_auth_group, field_name, getattr(auth_group, field_name))
        kaizen_auth_group.save()

    auth_permission_local_fields = [f.name for f in KaizenAuthPermission._meta.local_fields]
    for auth_permission in AuthPermission.objects.all():
        kaizen_auth_permission = KaizenAuthPermission()
        for field_name in auth_permission_local_fields:
            if field_name == 'group':
                kaizen_auth_group = KaizenAuthGroup.objects.get(id=getattr(auth_permission, 'group').id)
                setattr(kaizen_auth_permission, field_name, kaizen_auth_group)
            else:
                setattr(kaizen_auth_permission, field_name, getattr(auth_permission, field_name))
        kaizen_auth_permission.save()

    auth_user_local_fields = [f.name for f in KaizenAuthUser._meta.local_fields]
    for auth_user in AuthUser.objects.all():
        kaizen_auth_user = KaizenAuthUser()
        for field_name in auth_user_local_fields:
            setattr(kaizen_auth_user, field_name, getattr(auth_user, field_name))
        kaizen_auth_user.save()


def delete_table_records(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0006_auto_20160203_0906'),
    ]

    operations = [
        migrations.CreateModel(
            name='KaizenAuthGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.CharField(max_length=64, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('object', models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True)),
            ],
            options={
                'db_table': 'kaizen_auth_group',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='KaizenAuthPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, null=True, blank=True)),
                ('table_name', models.CharField(max_length=512, null=True, blank=True)),
                ('record_id', models.IntegerField(null=True, blank=True)),
                ('group', models.ForeignKey(blank=True, to='kaizen_api.KaizenAuthGroup', null=True)),
            ],
            options={
                'db_table': 'kaizen_auth_permission',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='KaizenAuthUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=64, null=True, blank=True)),
                ('last_name', models.CharField(max_length=64, null=True, blank=True)),
                ('email', models.CharField(max_length=512, null=True, blank=True)),
                ('password', models.CharField(max_length=512, null=True, blank=True)),
                ('registration_key', models.CharField(max_length=512, null=True, blank=True)),
                ('reset_password_key', models.CharField(max_length=512, null=True, blank=True)),
                ('registration_id', models.CharField(max_length=512, null=True, blank=True)),
                ('work_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('home_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('cell_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('work_position', models.CharField(max_length=32, null=True, blank=True)),
                ('work_title', models.CharField(max_length=32, null=True, blank=True)),
                ('photo', models.CharField(max_length=512, null=True, blank=True)),
                ('upload_photo', models.CharField(max_length=512, null=True, blank=True)),
                ('api_key', models.CharField(max_length=512, null=True, blank=True)),
                ('operations_access', models.CharField(max_length=1)),
                ('sales_access', models.CharField(max_length=1)),
                ('default_application', models.CharField(max_length=512, null=True, blank=True)),
                ('systems_access', models.CharField(max_length=1)),
                ('has_accepted_eula', models.CharField(max_length=1)),
                ('sales_admin', models.CharField(max_length=1)),
                ('object', models.ForeignKey(blank=True, to='kaizen_api.Objects', null=True)),
            ],
            options={
                'db_table': 'kaizen_auth_user',
                'managed': True,
            },
        ),
        # migrations.RunPython(copy_tables, reverse_code=delete_table_records),
    ]
