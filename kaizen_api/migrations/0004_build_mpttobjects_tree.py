# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, transaction
from django.core.exceptions import ObjectDoesNotExist
from kaizen_api.models import ObjectTypes, Objects, MpttObjects


def build_mptt_from_client_objects(apps, schema_editor):
    try:
        organization = ObjectTypes.objects.get(name__iexact='organization')
    except ObjectDoesNotExist:
        return

    with transaction.atomic():
        with MpttObjects.objects.delay_mptt_updates():
            for object in Objects.objects.filter(object_type=organization).order_by('id'):
                if object.parent:
                    parent = MpttObjects.objects.get(object=object.parent)
                    MpttObjects.objects.create(name=object.name, object=object, parent=parent)
                else:
                    MpttObjects.objects.create(name=object.name, object=object)


def clear_mptt_objects(apps, schema_editor):
    with transaction.atomic():
        with MpttObjects.objects.delay_mptt_updates():
            for mptt_object in MpttObjects.objects.all():
                mptt_object.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0003_create_mpttobjects'),
    ]

    operations = [
        migrations.RunPython(build_mptt_from_client_objects, reverse_code=clear_mptt_objects),
    ]
