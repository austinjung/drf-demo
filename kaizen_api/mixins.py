from django.http import Http404
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework import mixins, viewsets, status
from rest_framework.settings import api_settings
from rest_framework.serializers import ModelSerializer
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ParseError, ValidationError
from kaizen_api.models import *
import serializers
import json

LOOKUP_TO_CLASS_MAPPING = {
}


class FieldCacheMixin(ModelSerializer):
    """
    Cache the list of fields on the view class for GET requests only.
    Recreates the cache if the fields from `ExcludableFieldsMixin` are changed.
    """
    @property
    def fields(self):
        if self.context.get('request', None) and self.context['request'].method == 'GET':
            if hasattr(self.__class__, '_MNI_fields') and not getattr(self.__class__, '_MNI_invalid_field_cache', False):
                return getattr(self.__class__, '_MNI_fields')
            setattr(self.__class__, '_MNI_invalid_field_cache', False)
            setattr(self.__class__, '_MNI_fields', super(FieldCacheMixin, self).fields)
            return getattr(self.__class__, '_MNI_fields')
        else:
            # The request won't be passed when we're inspecting our serializers
            # in the Django shell, but this doesn't get reached otherwise
            return super(FieldCacheMixin, self).fields

    def get_field_names(self, declared_fields, info):
        """
        Override ModelSerializer.get_field_names().
        NOTE: both `fields` and `exclude` in the Meta class will be ignored.
        """
        field_names = []
        # Get parent class defined fields
        for cls in self.__class__.__bases__:
            field_names += getattr(cls, '_declared_fields', [])
        # And add fields defined on the current class
        field_names += declared_fields.keys()
        return field_names


class ExcludableFieldsMixin(object):
    """
    Exclude some fields from being serialized.
    Sets the class variable `_MNI_invalid_field_cache` if the excluded fields differ
    from the previous request.

    Specify fields to exclude with the query param `exclude_fields`.
    ex. http://localhost/api/notes/?exclude_fields=id&exclude_fields=created_date
    """
    _MNI_last_excluded_fields = []
    _MNI_invalid_field_cache = True

    def __init__(self, *args, **kwargs):
        context = kwargs.get('context')
        if context:
            exclude_fields = context['request'].query_params.getlist('exclude_fields')
        else:
            exclude_fields = []

        if exclude_fields != self.__class__._MNI_last_excluded_fields:
            self.__class__._MNI_invalid_field_cache = True

        self.__class__._MNI_last_excluded_fields = exclude_fields

        super(ExcludableFieldsMixin, self).__init__(*args, **kwargs)

        for field_name in exclude_fields:
            try:
                self.fields.pop(field_name)
            except (IndexError, KeyError):
                pass


class ValidationMixin(object):
    """
    Declares helpful utilities for validation functions and runs model-level
    validation on every serializer.
    """
    def get_parent_building(self):
        # i.e., /api/buildings/10/reports/1/ will return Building 10
        building_pk = self.context['view'].kwargs.get('building_pk', None)
        if not building_pk:
            raise ParseError('Could not find the parent building.')
        try:
            return Buildings.objects.get(pk=building_pk)
        except Buildings.DoesNotExist:
            raise NotFound('Building %s not found.' % str(building_pk))

    def get_pk(self):
        # i.e., /api/buildings/10/reports/1/ will return 1
        return self.context['view'].kwargs.get('pk', None)

    def translate_django_validation(self, instance):
        """
        Translate Django native ValidationError to DRF serializer
        ValidationError
        """
        try:
            instance.full_clean()
        except DjangoValidationError as e:
            if hasattr(e, 'error_dict'):
                # Convert field anf non-field errors
                errors = {}
                for key, val in e.error_dict.items():
                    if key == '__all__':
                        key = api_settings.NON_FIELD_ERRORS_KEY
                    messages = []
                    for error in val:
                        messages.extend(list(error))
                    errors[key] = messages
                raise ValidationError(errors)
            else:
                # Otherwise, it will be a list, which DRF handles properly
                raise

    def validate(self, attrs):
        """
        Run clean() on an instance of the model to run validation that is
        currently part of the model. Once we move validation to the API we
        no longer need to do this.
        """
        kwargs = self.context['view'].kwargs
        if 'building_pk' in kwargs:
            # Ensure that the bond_pk is to a real bond
            self.get_parent_building()

        if 'pk' in kwargs:
            # Updating an existing object
            instance = self.Meta.model.objects.get(pk=kwargs['pk'])
            for attribute, value in attrs.iteritems():
                setattr(instance, attribute, value)
            self.translate_django_validation(instance)
        else:
            # Creating a new object
            instance = self.Meta.model(**attrs)
            self.pre_validate(instance)
            # if not isinstance(instance, UserProfile):  # UserProfile case we don't do validate
            self.translate_django_validation(instance)
        return attrs

    def pre_validate(self, instance):
        pass


class APIVersioningMixin(object):
    def get_serializer_class(self):
        """
        Returns the version serializer for a specific api namespace if it exists.
        If it doesn't, the base class serializer is returned.
        """
        base_class = self.serializer_class
        if hasattr(self.request, 'version'):
            versioned_serializer = '%s_%s' % (base_class.__name__, self.request.version)
            return getattr(serializers, versioned_serializer, base_class)
        return base_class


class ChildMixin(object):
    def get_parent_filter(self, **kwargs):
        """Return a dictionary to be used to filter the queryset based on the parent object."""
        filter_dict = {}
        for key, value in kwargs.iteritems():
            try:
                value = int(value)
            except ValueError:
                raise Http404()
            if key == 'pk':
                # Object pk
                filter_dict['pk'] = value
            elif key.endswith('_pk'):
                # Parent pk
                name = key[:-3]
                # This will raise Http404 if the parent object doesn't exist
                get_object_or_404(LOOKUP_TO_CLASS_MAPPING[name], pk=value)
                filter_dict[name] = value
        return filter_dict

    def list(self, request, *args, **kwargs):
        """Return the serialized list of objects filtered on their parents."""
        # Both self.filter_queryset() and self.get_queryset() may be overwritten
        # and provide us with the filtering and ordering
        queryset = self.filter_queryset(self.get_queryset())

        # Add our own filters on the pk and parent_pk from the kwargs
        filtered_queryset = queryset.filter(**self.get_parent_filter(**kwargs))

        # Since we're overriding list(), we have to implement the pagination logic
        page = self.paginate_queryset(filtered_queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer_class()(filtered_queryset, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """Return the serialized object, or return 404 if it doesn't exist."""
        try:
            instance = self.queryset.get(**self.get_parent_filter(**kwargs))
        except ObjectDoesNotExist:
            raise Http404
        serializer = self.get_serializer_class()(instance, context={'request': request})
        return Response(serializer.data)


class TrackUserCreateModelMixin(mixins.CreateModelMixin):
    """
    Mixin that provides default `create()` actions with tracking user
    """
    def perform_create(self, serializer):
        with transaction.atomic(), reversion.create_revision():
            serializer.save()
            reversion.set_user(self.request.user)
            reversion.set_comment('created')


class TrackUserUpdateModelMixin(mixins.UpdateModelMixin):
    """
    Mixin that provides default `update()`, 'partial_update()` actions with tracking user
    """
    def perform_update(self, serializer):
        serializer.validated_data['method'] = self.request.method
        with transaction.atomic(), reversion.create_revision():
            serializer.save()
            reversion.set_user(self.request.user)
            reversion.set_comment(json.dumps(serializer.validated_data))


class TrackUserDestroyModelMixin(mixins.DestroyModelMixin):
    """
    Mixin that provides default `destroy()` actions with tracking user
    """
    def perform_destroy(self, instance):
        with transaction.atomic(), reversion.create_revision():
            instance.delete()
            reversion.set_user(self.request.user)
            reversion.set_comment('deleted')


class CreateRetrieveListViewSet(mixins.CreateModelMixin,
                                mixins.RetrieveModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    """
    A viewset that provides default `create()`, `retrieve()` and `list()` actions.
    """
    pass


class CreateViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    A viewset that provides default `create()`, `retrieve()` and `list()` actions.
    """
    pass


class TrackUserUpdateModelViewSet(mixins.CreateModelMixin,
                                  TrackUserUpdateModelMixin,
                                  mixins.RetrieveModelMixin,
                                  mixins.DestroyModelMixin,
                                  mixins.ListModelMixin,
                                  viewsets.GenericViewSet):
    """
    A viewset that provides default `update()`, `partial_update()` actions with tracking user and
    `create()`, `retrieve()`, `destroy()` and `list()` actions without tracking user
    """
    pass


class TrackUserModelViewSet(TrackUserCreateModelMixin,
                            mixins.RetrieveModelMixin,
                            TrackUserUpdateModelMixin,
                            TrackUserDestroyModelMixin,
                            mixins.ListModelMixin,
                            viewsets.GenericViewSet):
    """
    A viewset that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()` and `list()` actions with tracking user
    """
    pass

