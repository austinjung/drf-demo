# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
import reversion
from django.contrib.auth.models import *
from django.utils.translation import ugettext_lazy as _
from django.db import transaction


class MpttObjects(MPTTModel):
    name = models.CharField(max_length=128, blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        managed = True
        db_table = 'mptt_objects'
        app_label = 'kaizen_api'


class AlembicVersion(models.Model):
    version_num = models.CharField(max_length=32)

    class Meta:
        managed = True
        db_table = 'alembic_version'
        app_label = 'kaizen_api'


class AuthCas(models.Model):
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    service = models.CharField(max_length=512, blank=True, null=True)
    ticket = models.CharField(max_length=512, blank=True, null=True)
    renew = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_cas'
        app_label = 'kaizen_api'


class AuthEvent(models.Model):
    time_stamp = models.DateTimeField(blank=True, null=True)
    client_ip = models.CharField(max_length=512, blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    origin = models.CharField(max_length=512, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_event'
        app_label = 'kaizen_api'


class AuthGroup(models.Model):
    role = models.CharField(max_length=64, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_group'
        app_label = 'kaizen_api'

    def __str__(self):
        return self.role


@python_2_unicode_compatible
class DjangoGroup(models.Model):
    name = models.CharField(_('name'), max_length=80, unique=True)
    permissions = models.ManyToManyField('DjangoPermission',
        verbose_name=_('permissions'), blank=True)

    objects = GroupManager()

    class Meta:
        verbose_name = _('django group')
        verbose_name_plural = _('django groups')
        app_label = 'kaizen_api'

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)


class KaizenAuthGroup(models.Model):
    role = models.CharField(max_length=64, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'kaizen_auth_group'
        app_label = 'kaizen_api'

    def __str__(self):
        return self.role


class AuthMembership(models.Model):
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    group = models.ForeignKey('AuthGroup', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_membership'
        # unique_together = (('user_id', 'group_id'),)
        unique_together = (('user', 'group'),)
        app_label = 'kaizen_api'


class AuthPermission(models.Model):
    group = models.ForeignKey('AuthGroup', blank=True, null=True)
    name = models.CharField(max_length=64, blank=True, null=True)
    table_name = models.CharField(max_length=512, blank=True, null=True)
    record_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_permission'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


@python_2_unicode_compatible
class DjangoPermission(models.Model):
    name = models.CharField(_('name'), max_length=255)
    content_type = models.ForeignKey(ContentType)
    codename = models.CharField(_('codename'), max_length=100)
    objects = PermissionManager()

    class Meta:
        verbose_name = _('django permission')
        verbose_name_plural = _('django permissions')
        unique_together = (('content_type', 'codename'),)
        ordering = ('content_type__app_label', 'content_type__model',
                    'codename')
        app_label = 'kaizen_api'

    def __str__(self):
        return "%s | %s | %s" % (
            six.text_type(self.content_type.app_label),
            six.text_type(self.content_type),
            six.text_type(self.name))

    def natural_key(self):
        return (self.codename,) + self.content_type.natural_key()
    natural_key.dependencies = ['contenttypes.contenttype']


class KaizenAuthPermission(models.Model):
    group = models.ForeignKey('KaizenAuthGroup', blank=True, null=True)
    name = models.CharField(max_length=64, blank=True, null=True)
    table_name = models.CharField(max_length=512, blank=True, null=True)
    record_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'kaizen_auth_permission'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class AuthToken(models.Model):
    uuid = models.CharField(primary_key=True, max_length=512)
    user_id = models.IntegerField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'auth_token'
        app_label = 'kaizen_api'


class KaizenPermissionsMixin(models.Model):
    groups = models.ManyToManyField('DjangoGroup', verbose_name=_('groups'),
        blank=True, help_text=_('The groups this user belongs to. A user will '
                                'get all permissions granted to each of '
                                'their groups.'),
        related_name="user_set", related_query_name="user")
    user_permissions = models.ManyToManyField('DjangoPermission',
        verbose_name=_('user permissions'), blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="user_set", related_query_name="user")

    class Meta:
        abstract = True
        app_label = 'kaizen_api'

    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through their
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        # if self.is_active and self.is_superuser:
        if self.is_active and self.is_staff:
            return True

        # Otherwise we need to check the backends.
        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        # if self.is_active and self.is_superuser:
        if self.is_active and self.is_staff:
            return True

        return _user_has_module_perms(self, app_label)


@python_2_unicode_compatible
class KaizenAbstractBaseUser(models.Model):
    last_login = models.DateTimeField(_('last login'), blank=True, null=True)

    is_active = True

    REQUIRED_FIELDS = []

    class Meta:
        abstract = True
        app_label = 'kaizen_api'

    def get_username(self):
        "Return the identifying username for this User"
        return getattr(self, self.USERNAME_FIELD)

    def __str__(self):
        return self.get_username()

    def natural_key(self):
        return (self.get_username(),)

    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            self.save(update_fields=["password"])
        return check_password(raw_password, self.password, setter)

    def set_unusable_password(self):
        # Sets a value that will never be a valid hash
        self.password = make_password(None)

    def has_usable_password(self):
        return is_password_usable(self.password)

    def get_full_name(self):
        raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_full_name() method')

    def get_short_name(self):
        raise NotImplementedError('subclasses of AbstractBaseUser must provide a get_short_name() method.')

    def get_session_auth_hash(self):
        """
        Returns an HMAC of the password field.
        """
        key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
        return salted_hmac(key_salt, self.password).hexdigest()


class AuthUser(KaizenAbstractBaseUser, KaizenPermissionsMixin):
    first_name = models.CharField(max_length=64, blank=True, null=True)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    email = models.CharField(max_length=512, blank=True, null=True)
    password = models.CharField(max_length=512, blank=True, null=True)
    registration_key = models.CharField(max_length=512, blank=True, null=True)
    reset_password_key = models.CharField(max_length=512, blank=True, null=True)
    registration_id = models.CharField(max_length=512, blank=True, null=True)
    work_phone = models.CharField(max_length=32, blank=True, null=True)
    home_phone = models.CharField(max_length=32, blank=True, null=True)
    cell_phone = models.CharField(max_length=32, blank=True, null=True)
    work_position = models.CharField(max_length=32, blank=True, null=True)
    work_title = models.CharField(max_length=32, blank=True, null=True)
    photo = models.CharField(max_length=512, blank=True, null=True)
    upload_photo = models.CharField(max_length=512, blank=True, null=True)
    api_key = models.CharField(max_length=512, blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    operations_access = models.CharField(max_length=1)
    sales_access = models.CharField(max_length=1)
    default_application = models.CharField(max_length=512, blank=True, null=True)
    systems_access = models.CharField(max_length=1)
    has_accepted_eula = models.CharField(max_length=1)
    sales_admin = models.CharField(max_length=1)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    class Meta:
        managed = True
        db_table = 'auth_user'
        swappable = 'AUTH_USER_MODEL'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.email

    @property
    def username(self):
        return self.email

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class KaizenAuthUser(models.Model):
    first_name = models.CharField(max_length=64, blank=True, null=True)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    email = models.CharField(max_length=512, blank=True, null=True)
    password = models.CharField(max_length=512, blank=True, null=True)
    registration_key = models.CharField(max_length=512, blank=True, null=True)
    reset_password_key = models.CharField(max_length=512, blank=True, null=True)
    registration_id = models.CharField(max_length=512, blank=True, null=True)
    work_phone = models.CharField(max_length=32, blank=True, null=True)
    home_phone = models.CharField(max_length=32, blank=True, null=True)
    cell_phone = models.CharField(max_length=32, blank=True, null=True)
    work_position = models.CharField(max_length=32, blank=True, null=True)
    work_title = models.CharField(max_length=32, blank=True, null=True)
    photo = models.CharField(max_length=512, blank=True, null=True)
    upload_photo = models.CharField(max_length=512, blank=True, null=True)
    api_key = models.CharField(max_length=512, blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    operations_access = models.CharField(max_length=1)
    sales_access = models.CharField(max_length=1)
    default_application = models.CharField(max_length=512, blank=True, null=True)
    systems_access = models.CharField(max_length=1)
    has_accepted_eula = models.CharField(max_length=1)
    sales_admin = models.CharField(max_length=1)

    class Meta:
        managed = True
        db_table = 'kaizen_auth_user'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.email


class Buildings(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    address = models.CharField(max_length=512, blank=True, null=True)
    postal_code = models.CharField(max_length=512, blank=True, null=True)
    city = models.CharField(max_length=512, blank=True, null=True)
    prov_state = models.CharField(max_length=512, blank=True, null=True)
    country = models.CharField(max_length=32, blank=True, null=True)
    phone_number = models.CharField(max_length=512, blank=True, null=True)
    fax_number = models.CharField(max_length=512, blank=True, null=True)
    technical_contact = models.CharField(max_length=512, blank=True, null=True)
    technical_contact_phone = models.CharField(max_length=512, blank=True, null=True)
    technical_contact_email = models.CharField(max_length=512, blank=True, null=True)
    logo = models.CharField(max_length=512, blank=True, null=True)
    upload_logo = models.CharField(max_length=512, blank=True, null=True)
    building_type = models.CharField(max_length=512, blank=True, null=True)
    size_sqft = models.IntegerField(blank=True, null=True)
    size_occupancy = models.IntegerField(blank=True, null=True)
    licensed_tls = models.IntegerField(blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    weather_station = models.CharField(max_length=512, blank=True, null=True)
    timezone = models.ForeignKey('Timezones', blank=True, null=True)
    next_trend_instance = models.IntegerField(blank=True, null=True)
    next_structured_view_instance = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    size_sqmt = models.IntegerField(blank=True, null=True)
    preferred_building_size_unit = models.TextField(blank=True, null=True)  # This field type is a guess.
    building_type_0 = models.ForeignKey('BuildingTypes', db_column='building_type_id', blank=True, null=True)  # Field renamed because of name conflict.

    class Meta:
        verbose_name_plural = _('Buildings')
        managed = True
        db_table = 'buildings'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class BuildingTypePriceFactors(models.Model):
    estimator_building_type = models.CharField(unique=True, max_length=512)
    golden_standard_factor = models.DecimalField(max_digits=12, decimal_places=2)
    trend_log_vault_factor = models.DecimalField(max_digits=12, decimal_places=2)
    device_backup_vault_factor = models.DecimalField(max_digits=12, decimal_places=2)
    energy_factor = models.DecimalField(max_digits=12, decimal_places=2)
    integrity_factor = models.DecimalField(max_digits=12, decimal_places=2)

    class Meta:
        managed = True
        db_table = 'building_type_price_factors'
        app_label = 'kaizen_api'


class BuildingTypes(models.Model):
    type = models.CharField(unique=True, max_length=512)
    price_factor = models.ForeignKey('BuildingTypePriceFactors', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'building_types'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.type


class CalculatedTrendLogs(models.Model):
    trend_log_instance = models.IntegerField(blank=True, null=True)
    insight_rule_instance_id = models.IntegerField(blank=True, null=True)
    data_series_id = models.IntegerField(blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'calculated_trend_logs'
        unique_together = (('insight_rule_instance_id', 'data_series_id'),)
        app_label = 'kaizen_api'


class ChartEnergyMeters(models.Model):
    chart_instance = models.ForeignKey('ChartInstances', blank=True, null=True)
    energy_meter = models.ForeignKey('EnergyMeters', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'chart_energy_meters'
        app_label = 'kaizen_api'


class ChartInstances(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    data_set = models.ForeignKey('DataSets', blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    disabled_note = models.TextField(blank=True, null=True)
    chart = models.ForeignKey('Charts', blank=True, null=True)
    date_range = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'chart_instances'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Charts(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    disabled_note = models.TextField(blank=True, null=True)
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    author_notes = models.CharField(max_length=512, blank=True, null=True)
    dataflow = models.ForeignKey('Flows', db_column='dataflow', blank=True, null=True)
    sample_image = models.CharField(max_length=512, blank=True, null=True)
    lgx_url = models.CharField(max_length=512, blank=True, null=True)
    data_source = models.IntegerField(blank=True, null=True)
    lgx_file_name = models.CharField(max_length=512, blank=True, null=True)
    major_version = models.SmallIntegerField(blank=True, null=True)
    minor_version = models.SmallIntegerField(blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'charts'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class ChartsLibrary(models.Model):
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    user = models.ForeignKey(AuthUser, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    author_notes = models.CharField(max_length=512, blank=True, null=True)
    dataflow = models.ForeignKey('FlowsLibrary', db_column='dataflow', blank=True, null=True)
    sample_image = models.CharField(max_length=512, blank=True, null=True)
    avg_rating = models.IntegerField(blank=True, null=True)
    name = models.CharField(unique=True, max_length=128, blank=True, null=True)
    data_source = models.IntegerField(blank=True, null=True)
    lgx_url = models.CharField(max_length=512, blank=True, null=True)
    lgx_file_name = models.CharField(max_length=512, blank=True, null=True)
    major_version = models.SmallIntegerField(blank=True, null=True)
    minor_version = models.SmallIntegerField(blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'charts_library'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class ChartsLibraryRatings(models.Model):
    rating = models.IntegerField(blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    chart_library = models.ForeignKey('ChartsLibrary', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'charts_library_ratings'
        app_label = 'kaizen_api'


class Clients(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    address = models.CharField(max_length=512, blank=True, null=True)
    postal_code = models.CharField(max_length=512, blank=True, null=True)
    city = models.CharField(max_length=512, blank=True, null=True)
    prov_state = models.CharField(max_length=512, blank=True, null=True)
    country = models.CharField(max_length=32, blank=True, null=True)
    phone_number = models.CharField(max_length=512, blank=True, null=True)
    fax_number = models.CharField(max_length=512, blank=True, null=True)
    accounting_contact = models.ForeignKey('AuthUser', db_column='accounting_contact', blank=True, null=True)
    sales_contact = models.ForeignKey('AuthUser', db_column='sales_contact', blank=True, null=True, related_name='sales_contact_clients')
    technical_contact = models.ForeignKey('AuthUser', db_column='technical_contact', blank=True, null=True, related_name='technical_contact_clients')
    logo = models.CharField(max_length=512, blank=True, null=True)
    upload_logo = models.CharField(max_length=512, blank=True, null=True)
    logo_small = models.CharField(max_length=512, blank=True, null=True)
    upload_logo_small = models.CharField(max_length=512, blank=True, null=True)
    logo_footer = models.CharField(max_length=512, blank=True, null=True)
    upload_logo_footer = models.CharField(max_length=512, blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    gp_account_id = models.CharField(max_length=16, blank=True, null=True)
    partnership = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'clients'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class DataSets(models.Model):
    flow = models.ForeignKey('Flows', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'data_sets'
        app_label = 'kaizen_api'


class DataSetSeries(models.Model):
    data_set = models.ForeignKey('DataSets', blank=True, null=True)
    series_id = models.IntegerField(blank=True, null=True)
    series_override = models.TextField(blank=True, null=True)
    object_name = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'data_set_series'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.object_name


class DeviceSettings(models.Model):
    cucube_sitename = models.CharField(max_length=32, blank=True, null=True)
    dfirst = models.IntegerField(blank=True, null=True)
    dlast = models.IntegerField(blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)
    cucube_serial = models.CharField(max_length=32768, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'device_settings'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.cucube_sitename

class EnergyCustomUnits(models.Model):
    abbreviation = models.CharField(max_length=6, blank=True, null=True)
    name = models.CharField(max_length=32, blank=True, null=True)
    client = models.ForeignKey('Clients', blank=True, null=True)
    energy_type = models.IntegerField(blank=True, null=True)
    cascade = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'energy_custom_units'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class EnergyMeterConfig(models.Model):
    daily_display_interval = models.IntegerField(blank=True, null=True)
    # meter = models.ForeignKey('EnergyMeters', unique=True)
    meter = models.OneToOneField('EnergyMeters', default=None)

    class Meta:
        managed = True
        db_table = 'energy_meter_config'
        app_label = 'kaizen_api'


class EnergyMeters(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    energy_type = models.IntegerField(blank=True, null=True)
    map_object = models.CharField(max_length=512, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)
    suffix = models.CharField(max_length=512, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    meter_type = models.IntegerField(blank=True, null=True)
    is_utility = models.CharField(max_length=1, blank=True, null=True)
    # baseline_chart_type = models.CharField(max_length=-1, blank=True, null=True)
    baseline_chart_type = models.CharField(max_length=512, blank=True, null=True)
    target_adjustment = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'energy_meters'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class EnergyOatRef(models.Model):
    map_object = models.CharField(max_length=512)
    suffix = models.CharField(max_length=50, blank=True, null=True)
    meter_ref = models.ForeignKey('EnergyMeters', db_column='meter_ref', default=None)

    class Meta:
        managed = True
        db_table = 'energy_oat_ref'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.map_object


class Environments(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'environments'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Faults(models.Model):
    insight_rule = models.ForeignKey('InsightRules', blank=True, null=True)
    possible_fault_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'faults'
        app_label = 'kaizen_api'


class FaultsLibrary(models.Model):
    insight_rule_library = models.ForeignKey('InsightRulesLibrary', blank=True, null=True)
    possible_fault_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'faults_library'
        app_label = 'kaizen_api'


class Favorites(models.Model):
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    name = models.CharField(max_length=64, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)
    url = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'favorites'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Flows(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    designer_info = models.TextField(blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)
    insight_class = models.ForeignKey('InsightClasses', db_column='insight_class', blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    flow_binding = models.IntegerField(blank=True, null=True)
    is_compatible = models.CharField(max_length=1)

    class Meta:
        managed = True
        db_table = 'flows'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class FlowsLibrary(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    designer_info = models.TextField(blank=True, null=True)
    object = models.ForeignKey('Objects', blank=True, null=True)
    insight_class = models.ForeignKey('InsightClasses', db_column='insight_class', blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    flow_binding = models.IntegerField(blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'flows_library'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class GoldenStandardConfig(models.Model):
    object_type = models.CharField(max_length=128, blank=True, null=True)
    property = models.CharField(max_length=512, blank=True, null=True)
    display_name = models.CharField(max_length=512, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    property_group = models.IntegerField(blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    alternative_names = models.CharField(max_length=512, blank=True, null=True)
    property_sort = models.IntegerField(blank=True, null=True)
    display_value = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'golden_standard_config'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.display_name


class InsightClasses(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    insight_type = models.CharField(max_length=512, blank=True, null=True)
    category = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_classes'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightFilters(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_filters'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightFilterSettings(models.Model):
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    priority = models.ForeignKey('Priorities', blank=True, null=True)
    insight_filter = models.ForeignKey('InsightFilters', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_filter_settings'
        app_label = 'kaizen_api'


class InsightQuickFilters(models.Model):
    user = models.ForeignKey('AuthUser', default=None)
    name = models.CharField(max_length=32)
    insight_class = models.CharField(max_length=32, blank=True, null=True)
    state_snoozed = models.CharField(max_length=1, blank=True, null=True)
    state_active = models.CharField(max_length=1, blank=True, null=True)
    priority = models.CharField(max_length=16, blank=True, null=True)
    weekday = models.CharField(max_length=32, blank=True, null=True)
    date_range_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_quick_filters'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightRuleInstances(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    insight_message = models.CharField(max_length=512, blank=True, null=True)
    data_set = models.ForeignKey('DataSets', blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    disabled_note = models.TextField(blank=True, null=True)
    insight_rule = models.ForeignKey('InsightRules', blank=True, null=True)
    # schedule_id = models.AutoField()
    schedule_id = models.IntegerField()
    priority = models.ForeignKey('Priorities', blank=True, null=True)
    system_id = models.CharField(max_length=64, blank=True, null=True)
    needs_rerun = models.CharField(max_length=1)

    class Meta:
        managed = True
        db_table = 'insight_rule_instances'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightRules(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    disabled_note = models.TextField(blank=True, null=True)
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    author_notes = models.CharField(max_length=1028, blank=True, null=True)
    dataflow = models.ForeignKey(Flows, db_column='dataflow', blank=True, null=True)
    insight_trigger_series = models.IntegerField(blank=True, null=True)
    trigger_type = models.IntegerField(blank=True, null=True)
    date_range = models.IntegerField(blank=True, null=True)
    aggregation_interval = models.IntegerField(blank=True, null=True)
    major_version = models.SmallIntegerField(blank=True, null=True)
    minor_version = models.SmallIntegerField(blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)
    insight_message = models.CharField(max_length=512, blank=True, null=True)
    priority = models.ForeignKey('Priorities', blank=True, null=True)
    description = models.CharField(max_length=1024, blank=True, null=True)
    diagnosis = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_rules'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightRulesLibrary(models.Model):
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    author_notes = models.CharField(max_length=1028, blank=True, null=True)
    dataflow = models.ForeignKey('FlowsLibrary', db_column='dataflow', blank=True, null=True)
    insight_trigger_series = models.IntegerField(blank=True, null=True)
    trigger_type = models.IntegerField(blank=True, null=True)
    date_range = models.IntegerField(blank=True, null=True)
    aggregation_interval = models.IntegerField(blank=True, null=True)
    avg_rating = models.IntegerField(blank=True, null=True)
    name = models.CharField(unique=True, max_length=128, blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)
    major_version = models.SmallIntegerField(blank=True, null=True)
    minor_version = models.SmallIntegerField(blank=True, null=True)
    insight_message = models.CharField(max_length=512, blank=True, null=True)
    priority = models.ForeignKey('Priorities', blank=True, null=True)
    description = models.CharField(max_length=1024, blank=True, null=True)
    diagnosis = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_rules_library'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class InsightRulesLibraryRatings(models.Model):
    rating = models.IntegerField(blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    insight_library = models.ForeignKey('InsightRulesLibrary', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'insight_rules_library_ratings'
        app_label = 'kaizen_api'


class Inventory(models.Model):
    serial = models.CharField(max_length=512, blank=True, null=True)
    model = models.CharField(max_length=512, blank=True, null=True)
    mfgserial = models.CharField(max_length=512, blank=True, null=True)
    swversion = models.CharField(max_length=512, blank=True, null=True)
    date_acquired = models.DateField(blank=True, null=True)
    date_sold = models.DateField(blank=True, null=True)
    client_bought = models.ForeignKey('Clients', db_column='client_bought', blank=True, null=True, related_name='bought_inventories')
    client_deployed = models.ForeignKey('Clients', db_column='client_deployed', blank=True, null=True, related_name='deployed_inventories')
    netcfg = models.TextField(blank=True, null=True)
    oscred = models.TextField(blank=True, null=True)
    vpncred = models.TextField(blank=True, null=True)
    nabto = models.TextField(blank=True, null=True)
    history = models.TextField(blank=True, null=True)
    current_status = models.IntegerField(blank=True, null=True)
    coppercube_name = models.CharField(max_length=64, blank=True, null=True)
    physical_location = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'inventory'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.coppercube_name


class KaizenConfig(models.Model):
    key = models.CharField(primary_key=True, max_length=32)
    value = models.CharField(max_length=64)

    class Meta:
        managed = True
        db_table = 'kaizen_config'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.key


class MyBuildings(models.Model):
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'my_buildings'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.building.name


class Objects(models.Model):
    object_type = models.ForeignKey('ObjectTypes', blank=True, null=True)
    parent = models.ForeignKey('self', blank=True, null=True)
    name = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'objects'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class ObjectTypes(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'object_types'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Priorities(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'priorities'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class ReportArtifacts(models.Model):
    report = models.ForeignKey('Reports', blank=True, null=True)
    artifact = models.CharField(max_length=512, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'report_artifacts'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.report.name


class Reports(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    # schedule_id = models.AutoField()
    schedule_id = models.IntegerField()
    chart_instance = models.ForeignKey('ChartInstances', blank=True, null=True)
    enabled = models.CharField(max_length=1, blank=True, null=True)
    generation_timestamp = models.DateTimeField(blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True, related_name='building_reports')
    heading = models.CharField(max_length=512, blank=True, null=True)
    date_range = models.IntegerField(blank=True, null=True)
    client = models.ForeignKey('Clients', blank=True, null=True, related_name='client_reports')
    partner = models.ForeignKey('Clients', blank=True, null=True, related_name='partner_reports')
    created_at = models.DateTimeField(blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    accessed_at = models.DateTimeField(blank=True, null=True)
    created_by = models.ForeignKey('AuthUser', db_column='created_by', blank=True, null=True, related_name='created_reports')
    modified_by = models.ForeignKey('AuthUser', db_column='modified_by', blank=True, null=True, related_name='modified_reports')
    accessed_by = models.ForeignKey('AuthUser', db_column='accessed_by', blank=True, null=True, related_name='accessed_reports')

    class Meta:
        managed = True
        db_table = 'reports'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class ReportSubscriptions(models.Model):
    building = models.ForeignKey('Buildings', blank=True, null=True)
    group = models.ForeignKey('AuthGroup', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    report = models.ForeignKey('Reports', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'report_subscriptions'
        app_label = 'kaizen_api'


class SalesCucubeTiers(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    tl_count = models.IntegerField(blank=True, null=True)
    enterprise = models.CharField(max_length=1)
    cucube_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    cucube_lease_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    sql_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    sql_lease_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_cucube_tiers'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesKaizenPrices(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=3, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_kaizen_prices'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesLineItemsToGpSkus(models.Model):
    item_name = models.CharField(unique=True, max_length=512)
    official_item_name = models.CharField(max_length=512)
    gp_sku_number = models.CharField(max_length=512)

    class Meta:
        managed = True
        db_table = 'sales_line_items_to_gp_skus'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.item_name


class SalesMeterTiers(models.Model):
    meter_count = models.IntegerField()
    meter_price = models.DecimalField(max_digits=12, decimal_places=2)

    class Meta:
        managed = True
        db_table = 'sales_meter_tiers'
        app_label = 'kaizen_api'


class SalesOrderLineItems(models.Model):
    order = models.ForeignKey('SalesOrders', blank=True, null=True)
    yearly = models.CharField(max_length=1, blank=True, null=True)
    name = models.CharField(max_length=64)
    quantity = models.IntegerField()
    unit_price = models.DecimalField(max_digits=12, decimal_places=2)

    class Meta:
        managed = True
        db_table = 'sales_order_line_items'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesOrders(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    submitted_at = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    quote = models.ForeignKey('SalesQuotes', blank=True, null=True)
    status = models.CharField(max_length=64, blank=True, null=True)
    building = models.ForeignKey('Buildings', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_orders'
        app_label = 'kaizen_api'


class SalesQuoteLineItems(models.Model):
    quote = models.ForeignKey('SalesQuotes', blank=True, null=True)
    yearly = models.CharField(max_length=1, blank=True, null=True)
    name = models.CharField(max_length=64)
    quantity = models.IntegerField()
    unit_price = models.DecimalField(max_digits=12, decimal_places=2)

    class Meta:
        managed = True
        db_table = 'sales_quote_line_items'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesQuoteLostReasons(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_quote_lost_reasons'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesQuotes(models.Model):
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    status = models.CharField(max_length=32, blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    submitted = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    wizard_finished = models.CharField(max_length=1)
    project_name = models.CharField(max_length=64, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    building_name = models.CharField(max_length=64, blank=True, null=True)
    building_street1 = models.CharField(max_length=64, blank=True, null=True)
    building_street2 = models.CharField(max_length=64, blank=True, null=True)
    building_city = models.CharField(max_length=64, blank=True, null=True)
    building_state = models.CharField(max_length=64, blank=True, null=True)
    building_country = models.CharField(max_length=64, blank=True, null=True)
    building_zip = models.CharField(max_length=64, blank=True, null=True)
    building_sqft = models.IntegerField(blank=True, null=True)
    building_occupancy = models.IntegerField(blank=True, null=True)
    systems_advanced = models.CharField(max_length=1, blank=True, null=True)
    systems_skipped = models.CharField(max_length=1, blank=True, null=True)
    cucube_lease = models.CharField(max_length=1, blank=True, null=True)
    cucube_sql = models.CharField(max_length=1, blank=True, null=True)
    cucube_sizing = models.CharField(max_length=64, blank=True, null=True)
    service_tl_vault = models.CharField(max_length=1, blank=True, null=True)
    service_obj_vault = models.CharField(max_length=1, blank=True, null=True)
    service_energy = models.CharField(max_length=1, blank=True, null=True)
    service_golden_standard = models.CharField(max_length=1, blank=True, null=True)
    service_fdd = models.CharField(max_length=1, blank=True, null=True)
    partner_multiplier = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    target_margin = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    archive_reason = models.ForeignKey('SalesQuoteLostReasons', db_column='archive_reason', blank=True, null=True)
    archive_comments = models.TextField(blank=True, null=True)
    meters_count = models.IntegerField()
    client = models.ForeignKey('Clients', blank=True, null=True, related_name='client_quotes')
    timezone = models.ForeignKey('Timezones', blank=True, null=True)
    service_kaizen = models.CharField(max_length=1, blank=True, null=True)
    po_number = models.CharField(max_length=32, blank=True, null=True)
    address_code = models.CharField(max_length=15, blank=True, null=True)
    partner = models.ForeignKey('Clients', blank=True, null=True, related_name='partner_quotes')
    building_sqmt = models.IntegerField(blank=True, null=True)
    preferred_building_size_unit = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = True
        db_table = 'sales_quotes'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.project_name


class SalesQuotesCucubes(models.Model):
    quote = models.ForeignKey('SalesQuotes', blank=True, null=True)
    tier = models.ForeignKey('SalesCucubeTiers', blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_quotes_cucubes'
        app_label = 'kaizen_api'


class SalesQuotesSystemRows(models.Model):
    quote = models.ForeignKey('SalesQuotes', blank=True, null=True)
    system_type = models.ForeignKey('SalesSystemTypes', blank=True, null=True)
    description = models.CharField(max_length=64, blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    io_count = models.IntegerField(blank=True, null=True)
    tl_count = models.IntegerField(blank=True, null=True)
    obj_count = models.IntegerField(blank=True, null=True)
    fdd_enabled = models.CharField(max_length=1)

    class Meta:
        managed = True
        db_table = 'sales_quotes_system_rows'
        app_label = 'kaizen_api'


class SalesSpecialPricingRequests(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    quote = models.ForeignKey('SalesQuotes', blank=True, null=True)
    status = models.CharField(max_length=64, blank=True, null=True)
    request_comments = models.TextField(blank=True, null=True)
    response_comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_special_pricing_requests'
        app_label = 'kaizen_api'


class SalesSystemCategories(models.Model):
    identifier = models.CharField(max_length=32, blank=True, null=True)
    name = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'sales_system_categories'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class SalesSystemTypes(models.Model):
    category = models.ForeignKey('SalesSystemCategories', blank=True, null=True)
    name = models.CharField(max_length=32, blank=True, null=True)
    io_count = models.IntegerField(blank=True, null=True)
    tl_count = models.IntegerField(blank=True, null=True)
    obj_count = models.IntegerField(blank=True, null=True)
    add_by_default = models.CharField(max_length=1, blank=True, null=True)
    counts_always_editable = models.CharField(max_length=1)

    class Meta:
        managed = True
        db_table = 'sales_system_types'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Subscriptions(models.Model):
    building = models.ForeignKey('Buildings', blank=True, null=True)
    group = models.ForeignKey('AuthGroup', blank=True, null=True)
    user = models.ForeignKey('AuthUser', blank=True, null=True)
    insight_class = models.ForeignKey('InsightClasses', blank=True, null=True)
    priority = models.ForeignKey('Priorities', blank=True, null=True)
    delivery_method = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'subscriptions'
        app_label = 'kaizen_api'


class Timezones(models.Model):
    name = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'timezones'
        app_label = 'kaizen_api'

    def __unicode__(self):
        return self.name


class Web2PySessionShared(models.Model):
    locked = models.CharField(max_length=1, blank=True, null=True)
    client_ip = models.CharField(max_length=64, blank=True, null=True)
    created_datetime = models.DateTimeField(blank=True, null=True)
    modified_datetime = models.DateTimeField(blank=True, null=True)
    unique_key = models.CharField(max_length=64, blank=True, null=True)
    session_data = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'web2py_session_shared'
        app_label = 'kaizen_api'
