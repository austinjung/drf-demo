from django.core.urlresolvers import reverse

from django_mongoengine.document import Document, DynamicDocument
from mongoengine.queryset import QuerySet
from mongoengine import fields

import datetime


########################################################
# Util class for test

class WriteControllableQuerySet(QuerySet):

    def insert(self, doc_or_docs, load_bulk=True, write_concern=None):
        if self._collection._Collection__database._Database__client.address[0] in self._document._meta['writable_servers']:
            super(WriteControllableQuerySet, self).insert(doc_or_docs, load_bulk, write_concern)
        else:
            return []

    def delete(self, write_concern=None, _from_doc_delete=False, cascade_refs=None):
        if self._collection._Collection__database._Database__client.address[0] in self._document._meta['writable_servers']:
            super(WriteControllableQuerySet, self).delete(write_concern, _from_doc_delete, cascade_refs)
        else:
            return

    def update(self, upsert=False, multi=True, write_concern=None, full_result=False, **update):
        if self._collection._Collection__database._Database__client.address[0] in self._document._meta['writable_servers']:
            super(WriteControllableQuerySet, self).update(upsert, multi, write_concern, full_result, **update)
        else:
            return self._collection

    def modify(self, upsert=False, full_response=False, remove=False, new=False, **update):
        if self._collection._Collection__database._Database__client.address[0] in self._document._meta['writable_servers']:
            super(WriteControllableQuerySet, self).modify(upsert, full_response, remove, new, **update)
        else:
            return self._collection


class WriteControllableDynamicDocument(DynamicDocument):
    meta = {
        'abstract' : True,
        'queryset_class': WriteControllableQuerySet,
        'index_background': True,
        'index_drop_dups': True,
        'index_cls': False,
        'writable_servers': ['localhost', '127.0.0.1', 'kaizen.dev.coppertreeanalytics.com']  # 192.199.207.143 Kaizen QA Storage servers
    }

    @property
    def _qs(self):
        """
        Returns the queryset to use for updating / reloading / deletions
        """
        if not hasattr(self, '__objects'):
            self.__objects = WriteControllableQuerySet(self, self._get_collection())
        return self.__objects

    def save(self, *args, **kwargs):
        if self._qs._collection._Collection__database._Database__client.address[0] in self._meta['writable_servers']:
            super(WriteControllableDynamicDocument, self).save(*args, **kwargs)
        else:
            return self

    def cascade_save(self, *args, **kwargs):
        if self._qs._collection._Collection__database._Database__client.HOST in self._meta['writable_servers']:
            super(WriteControllableDynamicDocument, self).cascade_save(*args, **kwargs)
        else:
            return

    def delete(self, **write_concern):
        if self._qs._collection._Collection__database._Database__client.HOST in self._meta['writable_servers']:
            super(WriteControllableDynamicDocument, self).delete(**write_concern)
        else:
            return


########################################################
# Real Kaizen mongo DBs
# Collections in ctAdvising
class EmailAttachment(fields.EmbeddedDocument):
    type = fields.StringField(required=True)  # Image/pdf/....
    name = fields.StringField(required=True)  # cta.png/Insight Digest.pdf/...
    value = fields.StringField(required=True)


class EmailMessage(fields.EmbeddedDocument):
    body = fields.StringField(required=True)
    insight_class = fields.StringField()
    attachments = fields.ListField(fields.EmbeddedDocumentField('EmailAttachment'))
    timestamp = fields.DateTimeField(default=datetime.datetime.now, required=True)
    building_ids = fields.ListField(fields.IntField())
    priority = fields.IntField()
    to = fields.StringField(required=True)
    email_from = fields.StringField(required=True, db_field='from')
    subject = fields.StringField(required=True)


# class EmailFailedDelivery(WriteControllableDynamicDocument):
#     message = fields.EmbeddedDocumentField('EmailMessage')
#
#     meta = {
#         # 'allow_inheritance': True,
#         'collection': 'email_failed_delivery',
#         'index_background': True,
#         'index_drop_dups': True,
#         'index_cls': False,
#         'db_alias': 'advising'
#     }


class EmailSentLog(WriteControllableDynamicDocument):
    status = fields.StringField(required=True)  # Success/Fail
    expires = fields.DateTimeField(default=datetime.datetime.now() + datetime.timedelta(days=7), required=True)
    dt = fields.DateTimeField(default=datetime.datetime.now(), required=True)
    email = fields.StringField(required=True)  # austinjung@live.ca
    subject = fields.StringField(required=True, db_field='subject')

    meta = {
        # 'allow_inheritance': True,
        'collection': 'email_sent_log',
        'index_background': True,
        'index_drop_dups': True,
        'index_cls': False,
        'indexes': [
            {'fields': ['expires'], 'name': 'ttl', 'expireAfterSeconds': 0}  # Automatically delete after 7 days
        ],
        'db_alias': 'advising'
    }


class LatestObject(DynamicDocument):
    Building_ID = fields.IntField(required=True, db_field='l')
    Device_ID = fields.IntField(required=True, db_field='d')
    Object_Tags = fields.ListField(fields.StringField(), db_field='Tags')
    Object_Type = fields.StringField(required=True, db_field='t')
    Object_Identifier = fields.StringField(required=True)
    Object_Name = fields.StringField(required=True)
    Object_ID = fields.IntField(required=True, db_field='i')
    Object_Description = fields.StringField(db_field='Description')
    Node_Type = fields.StringField()
    Subordinate_List = fields.ListField(fields.StringField())
    Subordinate_Tags = fields.ListField(fields.StringField())
    Subordinate_Annotations = fields.ListField(fields.StringField())
    TimeStamp = fields.StringField(required=True, db_field='ts')

    meta = {
        # 'allow_inheritance': True,
        'collection': 'Latest',
        'index_background': True,
        'index_drop_dups': True,
        'index_cls': False,
        'indexes': [
            [('Building_ID', 1), ('Device_ID', 1), ('Object_Type', 1), ('Object_ID', 1)]
        ],
        'db_alias': 'objects'
    }


###########################################################
# Test DBs
class User(Document):
    email = fields.StringField(required=True)
    first_name = fields.StringField(max_length=50)
    last_name = fields.StringField(max_length=50)


class Comment(fields.EmbeddedDocument):
    created_at = fields.DateTimeField(default=datetime.datetime.now, required=True)
    author = fields.StringField(verbose_name="Name", max_length=255, required=True)
    body = fields.StringField(verbose_name="Comment", required=True)


class Post(Document):
    created_at = fields.DateTimeField(default=datetime.datetime.now, required=True)
    title = fields.StringField(max_length=255, required=True)
    slug = fields.StringField(max_length=255, required=True, primary_key=True)
    comments = fields.ListField(fields.EmbeddedDocumentField('Comment'))
    author = fields.ReferenceField(User)

    def get_absolute_url(self):
        return reverse('post', kwargs={"slug": self.slug})

    def __unicode__(self):
        return self.title

    @property
    def post_type(self):
        return self.__class__.__name__

    meta = {
        'indexes': ['-created_at', 'slug'],
        'ordering': ['-created_at'],
        'allow_inheritance': True
    }

    def save(self, *args, **kwargs):
        return super(Post, self).save(*args, **kwargs)


class BlogPost(Post):
    body = fields.StringField(required=True)


class Image(Post):
    image = fields.ImageField(required=True)


# class Quote(Post):
#     body = fields.StringField(required=True)
#     author = fields.StringField(verbose_name="Author Name", required=True, max_length=255)


class Music(Post):
    url = fields.StringField(max_length=100, verbose_name="Music Url", required=True)
    music_parameters = fields.DictField(verbose_name="Music Parameters", required=True)


class Video(Document):
    created_at = fields.DateTimeField(default=datetime.datetime.now, required=True)
    title = fields.StringField(max_length=255, required=True)
    slug = fields.StringField(max_length=255, required=True, primary_key=True)
    comments = fields.ListField(fields.EmbeddedDocumentField('Comment'))

    embed_code = fields.StringField(required=True)

    meta = {
        'indexes': ['-created_at', 'slug'],
        'ordering': ['-created_at'],
        'allow_inheritance': True,
        'db_alias': 'develop_db'
    }


class Page(DynamicDocument):
    title = fields.StringField(max_length=200, required=True)

    meta = {
        'allow_inheritance': True,
        'db_alias': 'develop_db'
    }


class ExpireTest(WriteControllableDynamicDocument):
    status = fields.StringField(required=True)  # Success/Fail
    expires = fields.DateTimeField(default=datetime.datetime.now() + datetime.timedelta(days=7), required=True)
    dt = fields.DateTimeField(default=datetime.datetime.now(), required=True)
    email = fields.StringField(required=True)  # austinjung@live.ca
    subject = fields.StringField(required=True)

    meta = {
        # 'allow_inheritance': True,
        'collection': 'email_sent_log',
        'index_background': True,
        'index_drop_dups': True,
        'index_cls': False,
        'indexes': [
            {'fields': ['expires'], 'name': 'ttl', 'expireAfterSeconds': 0}  # Automatically delete after 7 days
        ],
        'db_alias': 'develop_db'
    }


class EmailFailedDeliveryInDev(WriteControllableDynamicDocument):
    message = fields.EmbeddedDocumentField('EmailMessage')

    meta = {
        # 'allow_inheritance': True,
        'collection': 'email_failed_delivery',
        'index_background': True,
        'index_drop_dups': True,
        'index_cls': False,
        'db_alias': 'develop_db'
    }


