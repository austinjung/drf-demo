from rest_framework import viewsets, permissions

from kaizen_api.serializers import *
from kaizen_api.models import *
from kaizen_api.permissions import *
from kaizen_api.mixins import *
from django.db.models import Q
from mongoengine.queryset.visitor import Q as MongoQ


# views for postgres db
class BuildingsViewSet(TrackUserModelViewSet):
    queryset = Buildings.objects.all()
    serializer_class = BuildingsSerializer
    search_fields = ('name', 'technical_contact')
    filter_fields = ('city', 'building_type', 'preferred_building_size_unit')
    permission_classes = (permissions.IsAuthenticated, BuildingPermissions)

    def get_queryset(self):
        queryset = super(BuildingsViewSet, self).get_queryset()
        # return queryset.filter(id__in=self.request.user.kaizen_user.descendant_clients_building_ids)
        if 'descendant_clients_building_ids' in self.request.user.__dict__:
            return queryset.filter(id__in=self.request.user.descendant_clients_building_ids)
        else:
            descendant_client_object_ids = get_descendant_clients_from_mptt(self.request.user.object_id)
            descendant_clients_building_ids = [building.id for building in Buildings.objects.filter(object_id__in=descendant_client_object_ids)]
            return queryset.filter(id__in=descendant_clients_building_ids)


class ReportViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Reports.objects.all()
    serializer_class = ReportsSerializer


class TimezoneViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Timezones.objects.all()
    serializer_class = TimezoneSerializer


# views for mongo db
class EmailSentLogViewSet(viewsets.ModelViewSet):
    queryset = EmailSentLog.objects.all()
    serializer_class = EmailSentLogSerializer

    def get_queryset(self):
        queryset = super(EmailSentLogViewSet, self).get_queryset()
        search_key = self.request.query_params.get('search', None)
        if search_key:
            queryset = queryset.filter(subject__icontains=search_key)
        return queryset


# class EmailFailedDeliveryViewSet(viewsets.ModelViewSet):
#     queryset = EmailFailedDelivery.objects.all()
#     serializer_class = EmailFailedDeliverySerializer
#


class LatestObjectViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = LatestObject.objects.all()
    serializer_class = LatestObjectSerializer

    def get_queryset(self):
        queryset = super(LatestObjectViewSet, self).get_queryset()
        building_filter = self.request.query_params.get('Building_ID', None)
        if building_filter:
            queryset = queryset.filter(Building_ID=building_filter)
        device_filter = self.request.query_params.get('Device_ID', None)
        if device_filter:
            queryset = queryset.filter(Device_ID=device_filter)
        object_type_filter = self.request.query_params.get('Object_Type', None)
        if object_type_filter:
            queryset = queryset.filter(Object_Type=object_type_filter)
        object_filter = self.request.query_params.get('Object_ID', None)
        if object_filter:
            queryset = queryset.filter(Object_ID=object_filter)
        search_key = self.request.query_params.get('search', None)
        if search_key:
            # search_key='ahu'  # Working
            # search_key='ahu;cmd;cool;water;valve'  # Working
            # search_key='cmd;cool'  # Working
            # queryset = queryset.filter(MongoQ(Object_Tags__in=[search_key]) | MongoQ(Subordinate_Tags__in=[search_key]))  # Working
            queryset = queryset.filter(MongoQ(Object_Tags__icontains=search_key) | MongoQ(Subordinate_Tags__icontains=search_key))  # Working
        return queryset

