from kaizen_api.models import *
from rest_framework import serializers


class ReportsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reports
        fields = ('id', 'name', 'heading', 'created_at')


class TimezoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timezones
        fields = (
            'id', 'name'
        )


class BuildingsSerializer(serializers.HyperlinkedModelSerializer):
    building_reports = ReportsSerializer(many=True, read_only=True)
    class Meta:
        model = Buildings
        fields = ('id', 'name', 'address', 'postal_code', 'city', 'prov_state', 'country', 'size_sqft', 'size_sqmt',
                  'preferred_building_size_unit', 'building_reports', 'created_at', 'timezone')
        # fields = ('id', 'name', 'address', 'postal_code', 'city', 'prov_state', 'country', 'size_sqft', 'size_sqmt', 'preferred_building_size_unit', 'object', 'created_at')

