from kaizen_api.models import *
from rest_framework_mongoengine import serializers


class EmailSentLogSerializer(serializers.DynamicDocumentSerializer):
    class Meta:
        model = EmailSentLog
        fields = ('id', 'status', 'dt', 'email', 'subject')


class LatestObjectSerializer(serializers.DynamicDocumentSerializer):
    class Meta:
        model = LatestObject
        fields = (
            'id', 'Building_ID', 'Device_ID', 'Object_Tags', 'Object_Type', 'Object_Identifier', 'Object_Name', 'Object_ID', 'Object_Description',
            'Node_Type', 'Subordinate_List', 'Subordinate_Tags', 'Subordinate_Annotations', 'TimeStamp'
        )


class EmailAttachmentSerializer(serializers.EmbeddedDocumentSerializer):
    class Meta:
        model = EmailAttachment
        fields = ('type', 'name', 'value')


class EmailMessageSerializer(serializers.EmbeddedDocumentSerializer):
    attachments = EmailAttachmentSerializer(many=True)

    class Meta:
        model = EmailMessage
        fields = ('body', 'insight_class', 'attachments', 'building_ids', 'priority', 'to', 'email_from', 'subject', 'timestamp')


# class EmailFailedDeliverySerializer(serializers.DynamicDocumentSerializer):
#     message = EmailMessageSerializer(many=False)
#
#     class Meta:
#         model = EmailFailedDelivery
#         fields = ('id', 'message')
