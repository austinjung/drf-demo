from rest_framework import permissions
from kaizen_api.models import *


def get_descendant_clients_from_mptt(object_id):
    descendant_client_object_ids = []
    for mptt_object in MpttObjects.objects.get(object_id=object_id).get_descendants(include_self=True):
        descendant_client_object_ids.append(mptt_object.object.id)
    return descendant_client_object_ids


def is_sales_admin(user):
    """
    check if user is CopperTree sales admin
    """
    return user and user.sales_admin == 'T'


def has_sales_access(user):
    """
    check if user has sales access
    """
    return user and user.sales_access == 'T'


def is_client_admin(user):
    """
    check if user is a member of admin group
    """
    return user and ('Admins' in user.kaizen_groups)


class SalesAdminPermission(permissions.BasePermission):
    """
    Valid only if user is CopperTree sales admin with sales access
    """
    def has_permission(self, request, view):
        return request.user and is_sales_admin(request.user) and has_sales_access(request.user)


class SalesPermissions(permissions.BasePermission):
    """
    Valid only if user has sales access
    """
    def has_permission(self, request, view):
        return request.user and has_sales_access(request.user)


class BuildingPermissions(permissions.BasePermission):
    def has_object_permission(self, request, view, building):
        if not request.user:
            """If current request has no user, cannot access any building"""
            return False
        if is_sales_admin(request.user):
            """CopperTree Sales Admin can read/write/create/delete any building"""
            return True
        if building.id not in request.user.descendant_clients_building_ids:
            """If current building is not a building of descendant clients' buildings, cannot access any building"""
            return False
        if request.method in permissions.SAFE_METHODS:
            """If current building is a building of descendant clients' buildings, any user can read"""
            return True
        elif is_client_admin(request.user):
            """If current building is a building of descendant clients' buildings, only admin group user can write/create/delete"""
            return True
        return False
