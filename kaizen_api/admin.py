from django.contrib import admin
from kaizen_api.models import *
from reversion import VersionAdmin


class BuildingAdmin(VersionAdmin, admin.ModelAdmin):
    readonly_fields = ['id']
    fieldsets = [
        ('Info', {'fields': ['id', 'name', 'building_type_0', 'timezone']}),
        ('Address', {'fields': ['address', 'city', 'prov_state', 'country', 'postal_code']}),
        ('Contact',{'fields': ['technical_contact', 'technical_contact_email', 'technical_contact_phone']})
    ]
    list_display = (
        'id', 'name', 'building_type_0', 'address', 'city', 'prov_state', 'country', 'postal_code'
    )
    list_display_link = ("id",)
    list_filter = ['preferred_building_size_unit', 'building_type_0']
    search_fields = ['name', 'technical_contact']

    ignore_duplicate_revisions = True
    history_latest_first = True
    model = Buildings

admin.site.register(Buildings, BuildingAdmin)

class AuthUserAdmin(VersionAdmin, admin.ModelAdmin):
    readonly_fields = ['id']
    fieldsets = [
        ('Info', {'fields': ['id', 'first_name', 'last_name', 'email']}),
        ('Permission', {'fields': ['operations_access', 'sales_access', 'sales_admin', 'default_application', 'is_staff']}),
        ('Others',{'fields': ['has_accepted_eula', 'is_active']})
    ]
    list_display = (
        'id', 'first_name', 'last_name', 'email', 'operations_access', 'sales_access', 'sales_admin', 'default_application', 'is_staff', 'is_active'
    )
    list_display_link = ("id",)
    list_filter = ['default_application', 'is_staff', 'is_active']
    search_fields = ['first_name', 'last_name', 'email']

    ignore_duplicate_revisions = True
    history_latest_first = True
    model = AuthUser

admin.site.register(AuthUser, AuthUserAdmin)
