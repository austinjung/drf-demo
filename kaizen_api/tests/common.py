import json


class UtilitiesMixin(object):
    def endpoint_methods_test(self, endpoint, methods):
        uri = '/' + self.url + endpoint
        self.assertEqual(
            self.client.head(uri)._headers['allow'][1],
            methods,
            msg=uri
        )

    def assertFieldsExist(self, fields_list, all_fields):
        """Fails if any of fields_list is not in all_fields."""
        try:
            self.assertTrue(
                all(field in all_fields for field in fields_list)
            )
        except AssertionError:
            missing = set(fields_list).difference(set(all_fields))
            error = "Missing fields: {missing}".format(missing=", ".join(missing))
            raise AssertionError(error)

    def assertOnlyFields(self, fields_list, all_fields):
        """Fails if any of fields_list differs from all_fields."""
        missing = set(fields_list).difference(set(all_fields))
        extra = set(all_fields).difference(set(fields_list))
        try:
            self.assertEqual(
                (len(missing), len(extra)),
                (0, 0)
            )
        except AssertionError:
            error = "Fields don't match up: \n"
            if len(missing) > 0:
                error += "Missing fields: {missing}\n".format(missing=", ".join(missing))
            if len(extra) > 0:
                error += "Extra fields: {extra}\n".format(extra=", ".join(extra))
            raise AssertionError(error)

    def _post(self, uri, data, auth_key=None):
        """Uses self.client to POST to uri with JSON data."""
        if not auth_key:
            response = self.client.post(
                uri,
                data=json.dumps(data),
                content_type='application/json'
            )
        else:
            response = self.client.post(
                uri,
                data=json.dumps(data),
                HTTP_AUTHORIZATION=auth_key,
                content_type='application/json'
            )
        return response.status_code, json.loads(response.content)

    def _get(self, uri, auth_key=None, params=None):
        if not params:
            params = {}
        """Uses self.client to GET uri."""
        if not auth_key:
            response = self.client.get(
                uri,
                params,
                content_type='application/json',
            )
        else:
            response = self.client.get(
                uri,
                params,
                HTTP_AUTHORIZATION=auth_key,
                content_type='application/json',
            )
        return response.status_code, json.loads(response.content)

    def _delete(self, uri, auth_key=None):
        """Uses self.client to DELETE uri."""
        if not auth_key:
            response = self.client.delete(
                uri,
                content_type='application/json'
            )
        else:
            response = self.client.delete(
                uri,
                HTTP_AUTHORIZATION=auth_key,
                content_type='application/json'
            )
        try:
            return response.status_code, json.loads(response.content)
        except:
            return response.status_code, response.content

    def _patch(self, uri, data, auth_key=None):
        """Uses self.client to PATCH to uri with JSON data."""
        if not auth_key:
            response = self.client.patch(
                uri,
                data=json.dumps(data),
                content_type='application/json'
            )
        else:
            response = self.client.patch(
                uri,
                data=json.dumps(data),
                HTTP_AUTHORIZATION=auth_key,
                content_type='application/json'
            )
        return response.status_code, json.loads(response.content)

    def _put(self, uri, data, auth_key=None):
        """Uses self.client to PUT to uri with JSON data."""
        if not auth_key:
            response = self.client.put(
                uri,
                data=json.dumps(data),
                content_type='application/json'
            )
        else:
            response = self.client.put(
                uri,
                data=json.dumps(data),
                HTTP_AUTHORIZATION=auth_key,
                content_type='application/json'
            )
        return response.status_code, json.loads(response.content)


