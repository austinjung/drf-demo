from django.test import TestCase
from django.test.client import Client
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT, HTTP_403_FORBIDDEN
from django.utils import timezone
from django.utils.dateparse import parse_datetime
import datetime
from dateutil.tz import tzutc
from kaizen_api.models import *
from common import *


class BuildingTestCase(UtilitiesMixin, TestCase):
    def setup_buildings(self):
        self.object = Objects.objects.create(
            name='/',
            object_type_id=1
        )
        self.object1 = Objects.objects.create(
            name='Test Company',
            parent=self.object,
            object_type_id=1
        )
        self.build_mptt_object()
        self.timezone1 = Timezones.objects.create(
            name='Canada/Pacific'
        )
        self.building_type1 = BuildingTypes.objects.create(
            type='Building Type1'
        )
        self.building1 = Buildings.objects.create(
            name='Richmond Centre Mall',
            address='1400 - 6551 #3 Road',
            postal_code='V6Y 2B6',
            city='',
            prov_state='',
            country='',
            preferred_building_size_unit='sqmt',
            object=self.object1,
            timezone=self.timezone1,
            building_type=self.building_type1,
            created_at=timezone.now()
        )
        self.coppertree_sales_admin_api_key = "Token 2f145266cc494eb5"
        self.coppertree_sales_admin = AuthUser.objects.create(
            email='coppertree_sales_admin@coppertrreeanalytics.com',
            first_name='sales_admin',
            last_name='coppertree',
            sales_access='T',
            systems_access='T',
            sales_admin='T',
            api_key=self.coppertree_sales_admin_api_key[6:],
            object_id=self.object.id
        )

    def build_mptt_object(self):
        for object in Objects.objects.filter(object_type=1).order_by('id'):
            if object.parent:
                parent = MpttObjects.objects.get(object=object.parent)
                MpttObjects.objects.create(name=object.name, object=object, parent=parent)
            else:
                MpttObjects.objects.create(name=object.name, object=object)


    def setup_email_sent_logs(self):
        self.tz_now = timezone.now()
        for email_log in EmailSentLog.objects.all():
            email_log.delete()
        self.email_sent_logs = EmailSentLog.objects.create(
            status='Success',
            dt=self.tz_now,
            email='austinjung@live.ca',
            subject='CoppertreeHQ: Energy Insight Notification'
        )

    def setUp(self):
        self.setup_buildings()
        self.setup_email_sent_logs()
        self.client = Client()

    def test_get_buildings(self):
        # Get building list
        uri = '/buildings/'
        status, response = self._get(uri, auth_key=self.coppertree_sales_admin_api_key)
        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(response['count'], 1)
        self.assertEqual(response['next'], None)
        self.assertEqual(response['previous'], None)
        self.assertEqual(len(response['results']), 1)
        self.assertEqual(response['results'][0]['name'], 'Richmond Centre Mall')
        self.assertEqual(response['results'][0]['id'], self.building1.id)

    def test_get_building(self):
        # Get a building
        uri = '/buildings/%d/' % self.building1.id
        status, response = self._get(uri, auth_key=self.coppertree_sales_admin_api_key)
        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(response['name'], 'Richmond Centre Mall')
        self.assertEqual(response['preferred_building_size_unit'], 'sqmt')
        self.assertEqual(response['id'], self.building1.id)

    def test_update_building(self):
        # Update an existing building
        uri = '/buildings/%d/' % self.building1.id
        status, response = self._patch(
            uri,
            data={
                'preferred_building_size_unit': 'sqft'
            },
            auth_key=self.coppertree_sales_admin_api_key
        )
        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(response['name'], 'Richmond Centre Mall')
        self.assertEqual(response['preferred_building_size_unit'], 'sqft')
        self.assertEqual(response['id'], self.building1.id)

    def test_create_new_building(self):
        # Update an existing building
        uri = '/buildings/'
        status, response = self._post(
            uri,
            data={
                'name': 'New Building',
                'address': '33490 Kirk Avenue',
                'postal_code': 'V2S 5Y9',
                'city': 'Abbotsford',
                'preferred_building_size_unit': 'sqmt',
                'object': self.object1.id,
                'created_at': '2016-01-07T09:00:00+08:00'  # ISO 8601 datetime format
            },
            auth_key=self.coppertree_sales_admin_api_key
        )
        self.assertEqual(status, HTTP_201_CREATED)
        self.assertEqual(response['name'], 'New Building')
        self.assertEqual(response['postal_code'], 'V2S 5Y9')
        # Get building list
        uri = '/buildings/'
        status, response = self._get(uri, auth_key=self.coppertree_sales_admin_api_key)
        self.assertEqual(status, HTTP_200_OK)
        # self.assertEqual(response['count'], 2)
        self.assertEqual(response['count'], 1)
        self.assertEqual(response['next'], None)
        self.assertEqual(response['previous'], None)
        # self.assertEqual(len(response['results']), 2)
        self.assertEqual(len(response['results']), 1)
        self.assertEqual(response['results'][0]['name'], 'Richmond Centre Mall')
        # self.assertEqual(response['results'][1]['name'], 'New Building')

    def test_get_email_sent_logs(self):
        # Get email_sent_log list
        uri = '/email_sent_logs/'
        status, response = self._get(uri, auth_key=self.coppertree_sales_admin_api_key)
        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(response['count'], 1)
        self.assertEqual(len(response['results']), 1)

