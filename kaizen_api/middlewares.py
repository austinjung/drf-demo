from django.middleware.csrf import CsrfViewMiddleware


class KaizenCsrfViewMiddleware(CsrfViewMiddleware):

    def process_view(self, request, callback, callback_args, callback_kwargs):
        # token = request.META.get('HTTP_AUTHORIZATION')[:4]
        # if token.lower() == 'token':
        #     request.csrf_processing_done = True
        return super(KaizenCsrfViewMiddleware, self).process_view(request, callback, callback_args, callback_kwargs)


