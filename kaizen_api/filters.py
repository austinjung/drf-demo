from django.db.models.fields import FieldDoesNotExist
from django.db.models.fields.related import ManyToOneRel
from rest_framework.filters import OrderingFilter


# From https://github.com/tomchristie/django-rest-framework/issues/1005
#
class RelatedOrderingFilter(OrderingFilter):
    """
    Extends OrderingFilter to support ordering by fields in related models
    using the Django ORM __ notation
    """
    def is_valid_field(self, model, field):
        """
        Return true if the field exists within the model (or in the related
        model specified using the Django ORM __ notation)
        """
        components = field.split('__', 1)
        try:
            field = model._meta.get_field(components[0])

            # reverse relation
            if isinstance(field, ManyToOneRel):
                return self.is_valid_field(field.model, components[1])

            # foreign key
            if field.rel and len(components) == 2:
                return self.is_valid_field(field.rel.to, components[1])
            return True
        except FieldDoesNotExist:
            return False

    """
    Extends OrderingFilter to support ordering by fields in related models
    using the Django ORM __ notation
    """
    def is_valid_mongo_field(self, document_meta, field):
        """
        Return true if the field exists within the model (or in the related
        model specified using the Django ORM __ notation)
        """
        components = field.split('__', 1)
        try:
            document_meta._fields[components[0]]
            return True
        except FieldDoesNotExist:
            return False

    def remove_invalid_fields(self, queryset, ordering, view):
        if '_document' in queryset.__dict__:
            return [term for term in ordering
                    if self.is_valid_mongo_field(queryset._document, term.lstrip('-'))]
        elif 'model' in queryset.__dict__:
            return [term for term in ordering
                    if self.is_valid_field(queryset.model, term.lstrip('-'))]
        else:
            return []

