import sys
import os

sys.path.extend([os.getcwd()])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_demo.settings")

import django
django.setup()

from kaizen_api.models import *
import timeit


abs_system = None
root = None
descendant_clients = None
sql = """WITH RECURSIVE child AS
        (
            SELECT * FROM objects WHERE id = %s
            UNION ALL
            SELECT d.* FROM objects as d
            JOIN child as sd ON (d.parent_id = sd.id)
                WHERE (d.object_type_id = 1)
        )
        SELECT
            child.id AS id,
            child.object_type_id AS object_type_id,
            clients.name AS client_name,
            child.parent_id AS parent_id,
            clients.id AS client_id
        FROM child
        INNER JOIN clients ON child.id = clients.object_id
        WHERE child.object_type_id = 1
        ORDER BY LOWER(clients.name);"""

def get_descendant_clients_from_old_tree(object_id):
    global descendant_clients
    descendant_clients = []
    for client in Clients.objects.raw(sql, [str(object_id)]):
        descendant_clients.append(client)


def get_descendant_clients_from_mptt(object_id):
    global descendant_clients
    descendant_clients = []
    for client in MpttObjects.objects.get(object_id=object_id).get_descendants(include_self=True):
        descendant_clients.append(client)


def get_descendant_clients_tree_from_mptt(object_id):
    global descendant_clients
    descendant_clients = []
    for client in MpttObjects.objects.get(object_id=object_id).get_descendants(include_self=True):
        descendant_clients.append(client)


def print_descendants_name(descendants):
    clients = []
    for descendant in descendants:
        clients.append(descendant.name)
    print clients


if __name__ == '__main__':
    abs_system = Clients.objects.get(name='ABS Systems')
    print 'List of descendants of ABS System'
    get_descendant_clients_from_old_tree(abs_system.object_id)
    print [client.client_name for client in descendant_clients]

    print 'List of descendants of root'
    get_descendant_clients_from_mptt(abs_system.object_id)
    print_descendants_name(descendant_clients)

    root = Clients.objects.get(name='/')
    print 'Get Root object ID %d' % root.object_id
    print 'Print time to get all descendant clients of Root using objects tree'
    print([timeit.timeit('get_descendant_clients_from_old_tree(root.object_id)', number=1, setup='from __main__ import get_descendant_clients_from_old_tree, root') for x in range(0, 10)])
    print 'Number of root clients is %d' % len(descendant_clients)

    print 'Print time to get all descendant clients of Root using mptt'
    print([timeit.timeit('get_descendant_clients_from_mptt(root.object_id)', number=1, setup='from __main__ import get_descendant_clients_from_mptt, root') for x in range(0, 10)])
    print 'Number of root clients is %d' % len(descendant_clients)
