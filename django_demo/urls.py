"""django_demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers as drf_routers
from rest_framework_mongoengine import routers as mongo_routers
from kaizen_api.views import *


drf_router = drf_routers.SimpleRouter()
drf_router.register(r'buildings', BuildingsViewSet)
drf_router.register(r'reports', ReportViewSet)
drf_router.register(r'timezones', TimezoneViewSet)
mongo_router = mongo_routers.SimpleRouter()
mongo_router.register(r'email_sent_logs', EmailSentLogViewSet)
# mongo_router.register(r'email_failed_deliveries', EmailFailedDeliveryViewSet)
mongo_router.register(r'latest_objects', LatestObjectViewSet)


urlpatterns = [
    url(r'^', include(drf_router.urls)),
    url(r'^', include(mongo_router.urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
