# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, transaction
from kaizen_api.models import *
import reversion
from django.db import connection


def create_initial_revisions(apps, schema_editor):
    with transaction.atomic(), reversion.create_revision():
        reversion.set_comment('Initial revision')
        for group in AuthGroup.objects.all():
            group.save()
        for membership in AuthMembership.objects.all():
            membership.save()
        for permission in AuthPermission.objects.all():
            permission.save()
        for token in AuthToken.objects.all():
            token.save()
        for user in AuthUser.objects.all():
            user.save()
        for price_factor in BuildingTypePriceFactors.objects.all():
            price_factor.save()
        for type in BuildingTypes.objects.all():
            type.save()
        for building in Buildings.objects.all():
            building.save()
        for chart in ChartInstances.objects.all():
            chart.save()
        for chart in Charts.objects.all():
            chart.save()
        for chart in ChartsLibrary.objects.all():
            chart.save()
        for client in Clients.objects.all():
            client.save()
        for setting in DeviceSettings.objects.all():
            setting.save()
        for unit in EnergyCustomUnits.objects.all():
            unit.save()
        for config in EnergyMeterConfig.objects.all():
            config.save()
        for meter in EnergyMeters.objects.all():
            meter.save()
        for ref in EnergyOatRef.objects.all():
            ref.save()
        for env in Environments.objects.all():
            env.save()
        for fault in Faults.objects.all():
            fault.save()
        for fault in FaultsLibrary.objects.all():
            fault.save()
        for favorite in Favorites.objects.all():
            favorite.save()
        for flow in Flows.objects.all():
            flow.save()
        for flow in FlowsLibrary.objects.all():
            flow.save()
        for config in GoldenStandardConfig.objects.all():
            config.save()
        for insight in InsightClasses.objects.all():
            insight.save()
        for insight in InsightFilterSettings.objects.all():
            insight.save()
        for insight in InsightFilters.objects.all():
            insight.save()
        for insight in InsightQuickFilters.objects.all():
            insight.save()
        for insight in InsightRuleInstances.objects.all():
            insight.save()
        for insight in InsightRules.objects.all():
            insight.save()
        for insight in InsightRulesLibrary.objects.all():
            insight.save()
        for insight in InsightRulesLibraryRatings.objects.all():
            insight.save()
        for inventory in Inventory.objects.all():
            inventory.save()
        for config in KaizenConfig.objects.all():
            config.save()
        for building in MyBuildings.objects.all():
            building.save()
        for type in ObjectTypes.objects.all():
            type.save()
        for o in Objects.objects.all():
            o.save()
        for p in Priorities.objects.all():
            p.save()
        for r in ReportArtifacts.objects.all():
            r.save()
        for r in ReportSubscriptions.objects.all():
            r.save()
        for r in Reports.objects.all():
            r.save()
        for s in SalesCucubeTiers.objects.all():
            s.save()
        for s in SalesKaizenPrices.objects.all():
            s.save()
        for s in SalesLineItemsToGpSkus.objects.all():
            s.save()
        for s in SalesMeterTiers.objects.all():
            s.save()
        for s in SalesOrderLineItems.objects.all():
            s.save()
        for s in SalesOrders.objects.all():
            s.save()
        for s in SalesQuoteLineItems.objects.all():
            s.save()
        for s in SalesQuoteLostReasons.objects.all():
            s.save()
        for s in SalesQuotes.objects.all():
            s.save()
        for s in SalesQuotesCucubes.objects.all():
            s.save()
        for s in SalesQuotesSystemRows.objects.all():
            s.save()
        for s in SalesSpecialPricingRequests.objects.all():
            s.save()
        for s in SalesSystemCategories.objects.all():
            s.save()
        for s in SalesSystemTypes.objects.all():
            s.save()
        for s in Subscriptions.objects.all():
            s.save()
        for t in Timezones.objects.all():
            t.save()


def clear_initial_revisions(apps, schema_editor):
    cursor = connection.cursor()
    cursor.execute('TRUNCATE reversion_revision CASCADE;')
    cursor.execute('ALTER SEQUENCE reversion_revision_id_seq RESTART WITH 1;')
    cursor.execute('ALTER SEQUENCE reversion_version_id_seq RESTART WITH 1;')


class Migration(migrations.Migration):

    dependencies = [
        ('kaizen_api', '0005_auto_20160223_2342'),
    ]

    operations = [
        migrations.RunPython(create_initial_revisions, reverse_code=clear_initial_revisions),
    ]
