import sys
import os
from Cupric.services.trends import TrendLogService
import ct_config
import pandas as pd

sys.path.extend([os.getcwd()])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_demo.settings")

import django
django.setup()

from kaizen_api.models import *

quote = SalesQuotes.objects.filter(status='submitted').order_by('-id').first()
print quote.project_name
quote.project_name += ' modified'
quote.project_name = quote.project_name[:64]
quote.save()
quote = SalesQuotes.objects.filter(status='submitted').order_by('-id').first()
print quote.project_name

users = User.objects.filter(email='austinjung@live.ca')
if len(users) == 0:
    user = User(email='austinjung@live.ca', first_name='Austin', last_name='Jung')
    user.save()
else:
    user = users[0]

blog_posts = BlogPost.objects.filter(slug='http://iis-tools.net')
if len(blog_posts) == 0:
    blog_post = BlogPost(title='Using MongoEngine', body='Sample to user hybrid django with Postgres and Mongo', slug='http://iis-tools.net')
else:
    blog_post = blog_posts[0]
    blog_post.title += ' modified'
blog_post.comments = [Comment(author='Austin Jung', body='My comment')]
blog_post.author = user
blog_post.title = blog_post.title[:255]
blog_post.save()
blog_post.save(using='develop_db')
blog_post = BlogPost.objects.get(slug='http://iis-tools.net')
print blog_post.title

posts = Post.objects.filter(slug='PostTest')
if len(posts) == 0:
    post = Post(title='Using MongoEngine', slug='PostTest')
else:
    post = posts[0]
    post.title += ' modified'
post.author = user
post.title = post.title[:255]
post.save()
post.save(using='develop_db')
post = Post.objects.get(slug='PostTest')
print post.title

video_posts = Video.objects.using('develop_db').filter(slug='VideoTest')
if len(video_posts) == 0:
    video_post = Video(title='Using MongoEngine', slug='VideoTest', embed_code='Embeded Code')
else:
    video_post = video_posts[0]
    video_post.title += ' modified'
video_post.title = video_post.title[:255]
video_post.save()
video_post = Video.objects.using('develop_db').get(slug='VideoTest')
print video_post.title

page = Page(title='Using MongoEngine')
page.tags = ['mongodb', 'mongoengine']
page.save()
print Page.objects(tags='mongoengine').count()

expire = ExpireTest(status='Success', email='austinjung@live.ca', subject='ExpireAt ID Test')
expire.save()
print expire.subject
ExpireTest.objects.filter(email='austinjung@live.ca').update(subject='ExpireAt ID Queryset Update Test')

log = EmailSentLog(status='Success', email='austinjung@live.ca', subject='ExpireAt ID Test')
log.save()

# log_in_qa = EmailFailedDelivery.objects.first()
# log_in_dev = EmailFailedDeliveryInDev()
# log_in_dev.message = log_in_qa.message
# log_in_dev.save()

# config = ct_config.load('kaizen.yaml')
# mongodb = config.get('connectionStrings', 'mongodb')
# trend_log_service = TrendLogService(mongodb)
# sample = trend_log_service.get_latest_sample('2156.0.TL10')
# i = 0

print 'Finish successfully'
